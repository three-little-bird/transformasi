/*
=========================================
|                                       |
|           Scroll To Top               |
|                                       |
=========================================
*/ 
$('.scrollTop').click(function() {
    $("html, body").animate({scrollTop: 0});
});


$('.navbar .dropdown.notification-dropdown > .dropdown-menu, .navbar .dropdown.message-dropdown > .dropdown-menu ').click(function(e) {
    e.stopPropagation();
});

/*
=========================================
|                                       |
|       Multi-Check checkbox            |
|                                       |
=========================================
*/

function checkall(clickchk, relChkbox) {

    var checker = $('#' + clickchk);
    var multichk = $('.' + relChkbox);


    checker.click(function () {
        multichk.prop('checked', $(this).prop('checked'));
    });    
}


/*
=========================================
|                                       |
|           MultiCheck                  |
|                                       |
=========================================
*/

/*
    This MultiCheck Function is recommanded for datatable
*/

function multiCheck(tb_var) {
    tb_var.on("change", ".chk-parent", function() {
        var e=$(this).closest("table").find("td:first-child .child-chk"), a=$(this).is(":checked");
        $(e).each(function() {
            a?($(this).prop("checked", !0), $(this).closest("tr").addClass("active")): ($(this).prop("checked", !1), $(this).closest("tr").removeClass("active"))
        })
    }),
    tb_var.on("change", "tbody tr .new-control", function() {
        $(this).parents("tr").toggleClass("active")
    })
}

/*
=========================================
|                                       |
|           MultiCheck                  |
|                                       |
=========================================
*/

function checkall(clickchk, relChkbox) {

    var checker = $('#' + clickchk);
    var multichk = $('.' + relChkbox);


    checker.click(function () {
        multichk.prop('checked', $(this).prop('checked'));
    });    
}

/*
=========================================
|                                       |
|               Tooltips                |
|                                       |
=========================================
*/

$('.bs-tooltip').tooltip();

/*
=========================================
|                                       |
|               Popovers                |
|                                       |
=========================================
*/

$('.bs-popover').popover();


/*
================================================
|                                              |
|               Rounded Tooltip                |
|                                              |
================================================
*/

$('.t-dot').tooltip({
    template: '<div class="tooltip status rounded-tooltip" role="tooltip"><div class="arrow"></div><div class="tooltip-inner"></div></div>'
})


/*
================================================
|            IE VERSION Dector                 |
================================================
*/

function GetIEVersion() {
  var sAgent = window.navigator.userAgent;
  var Idx = sAgent.indexOf("MSIE");

  // If IE, return version number.
  if (Idx > 0) 
    return parseInt(sAgent.substring(Idx+ 5, sAgent.indexOf(".", Idx)));

  // If IE 11 then look for Updated user agent string.
  else if (!!navigator.userAgent.match(/Trident\/7\./)) 
    return 11;

  else
    return 0; //It is not IE
}

function startPageLoading() {
    //
}

function stopPageLoading() {
    //
}

function loadUrlTo(tmpTarget, tmpUrl) {
    startPageLoading();
    $.ajax({
        type: 'GET',
        cache: false,
        url: tmpUrl,
        dataType: 'HTML',
        success: function (response) {
            stopPageLoading();
            $(tmpTarget).empty().append(response);
            // handleInit()
        },
        error: function (xhr, status, error) {
            stopPageLoading();
            $(tmpTarget).empty().append(`<font color='red'>`+xhr.statusText+`</font>`);
        }
    });
}

function addModal(url, judul) {
    $('#add-modal-box .modal-body').html(`<br><div class='text-center'><i class="fa fa-circle-o-notch fa-spin"></i></div>`);
    loadUrlTo('#add-modal-box .modal-body', url);
    $('#add-modal-box .modal-title').html(judul);
    $('#add-modal-box').modal('show');
}

function initScript() {
    $('body').on('click', '[data-toggle=popajax]', function(e) {
        e.preventDefault();
        if (e.which != 1) return false;

        var url = $(this).attr('data-url');
        var judul = $(this).attr('data-title') ?? '&nbsp;';

        addModal(url, judul);
    });

    $('body').on('click', '[data-toggle=popconfirm]', function(e) {
        e.preventDefault();
        if (e.which != 1) return false;
        var elem = $(this);
        Swal.fire({
          title: 'Apakah kamu yakin?',
          text: "Aksi ini tidak dapat diulang!",
          type: 'warning',
          showCancelButton: true,
          confirmButtonText: 'Confirm',
          padding: '2em'
        }).then(function(result) {
            if (result.value) {
              window.location = elem.attr('data-url');
            }
        });
    });
}

initScript();