<?php

return [
    'title' => 'Transformasi | Networked Think Tank',
    'newly-news-title' => 'Latest News & Activities',
    'newly-news-desc' => 'Help today because tomorrow you may be the one who needs more helping!',
    'read-more' => 'Read more',
    'view-all-article' => 'View All Articles',
    'recent-publication' => 'Recent Publications',
    'view-all-publication' => 'View All Publications',
    'social-media-title' => 'Social Media',
    'social-media-desc' => 'Help today because tomorrow you may be the one who needs more helping!',
    'policy-consulting-service-title' => 'Policies Consultation Services',
    'policy-consulting-service-desc' => 'We provide consulting services to public sector clients, such as the central governments and local governments to conduct evidence and fact-based research and policy formulation. We also carry out concept development and management of CSR (Corporate Social Responsibility) funds.',
    'join-us-title' => 'Join Us',
    'join-us-desc' => "Are you interested pursuing a career in inclusive development? Some people have contributed their time and skillset to make it happen, now it is your turn.",
    'join-us-subdesc' => "Working at Transformasi is not merely a job, but you will join with professional team and scholars from top-ranked local and international universities. You will be part of an environment that supports inclusive development.",
    'join' => 'Join Us',
    'join-img' => 'assets/img/join/Eng.png',
];
