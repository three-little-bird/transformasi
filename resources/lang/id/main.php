<?php

return [
    'title' => 'Transformasi | Jaringan Para Pemikir',
    'newly-news-title' => 'Berita & Kegiatan Terkini',
    'newly-news-desc' => 'Tolong sekarang karna suatu saat kamu akan membutuhkannya juga!',
    'read-more' => 'Selengkapnya',
    'view-all-article' => 'Lihat Semua Artikel',
    'recent-publication' => 'Publikasi Terkini',
    'view-all-publication' => 'Lihat Semua Publikasi',
    'social-media-title' => 'Media Sosial',
    'social-media-desc' => 'Tolong sekarang karna suatu saat kamu akan membutuhkannya juga!',
    'policy-consulting-service-title' => 'Layanan Konsultasi Kebijakan',
    'policy-consulting-service-desc' => 'Kami menyediakan layanan konsultasi kepada klien di sektor publik seperti  kementerian dan pemerintah daerah dalam melakukan riset dan formulasi kebijakan yang berdasarkan pada bukti dan fakta, pengembangan konsep dan pengelolaan dana CSR perusahaan.',
    'join-us-title' => 'Bergabung  Bersama Kami',
    'join-us-desc' => 'Berminat untuk berkarir di bidang pembangunan inklusif? Beberapa orang sudah memberikan waktu dan bakat mereka untuk mewujudkannya, sekarang giliran Anda.',
    'join-us-subdesc' => 'Bekerja di Transformasi bukan hanya sebagai pekerjaan, Anda akan bergabung dengan tim profesional dan akademisi dari universitas lokal dan internasional terkemuka. Anda akan menjadi bagian dari lingkungan yang mendukung terwujudnya pembangunan yang inklusif.',
    'join' => 'Bergabung',
    'join-img' => 'assets/img/join/Ind.png',
];
