@php
    if (!isset($menuActive)) {
        $menuActive = null;
    }
@endphp
<!DOCTYPE html>
<html lang="id">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no">
    <title>Transformasi Admin</title>
    <link rel="shortcut icon" type="image/x-icon" href="{{ asset('favicon.ico') }}">
    <link href="{{ asset('dashboard/assets/css/loader.css') }}" rel="stylesheet" type="text/css" />
    <script src="{{ asset('dashboard/assets/js/loader.js') }}"></script>

    <!-- BEGIN GLOBAL MANDATORY STYLES -->
    <link href="https://fonts.googleapis.com/css?family=Quicksand:400,500,600,700&display=swap" rel="stylesheet">
    <link href="{{ asset('dashboard/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('dashboard/assets/css/plugins.css') }}" rel="stylesheet" type="text/css" />
    <!-- END GLOBAL MANDATORY STYLES -->

    <!-- BEGIN PAGE LEVEL PLUGINS/CUSTOM STYLES -->
    {{-- <link rel="stylesheet" href="{{ asset('dashboard/plugins/sweetalerts/sweetalert2.min.css') }}"> --}}
    <link rel="stylesheet" href="{{ asset('dashboard/plugins/font-icons/fontawesome/css/solid.css') }}">
    <link rel="stylesheet" href="{{ asset('dashboard/plugins/font-icons/fontawesome/css/fontawesome.css') }}">
    {{-- <link href="{{ asset('dashboard/plugins/apex/apexcharts.css') }}" rel="stylesheet" type="text/css"> --}}
    {{-- <link href="{{ asset('dashboard/assets/css/dashboard/dash_2.css') }}" rel="stylesheet" type="text/css" /> --}}
    <!-- END PAGE LEVEL PLUGINS/CUSTOM STYLES -->

</head>
<body class="alt-menu sidebar-noneoverflow">
    <!-- BEGIN LOADER -->
    <div id="load_screen"> <div class="loader"> <div class="loader-content">
        <div class="spinner-grow align-self-center"></div>
    </div></div></div>
    <!--  END LOADER -->

    <!--  BEGIN NAVBAR  -->
    <div class="header-container">
        <header class="header navbar navbar-expand-sm">

            <a href="javascript:void(0);" class="sidebarCollapse" data-placement="bottom"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-menu"><line x1="3" y1="12" x2="21" y2="12"></line><line x1="3" y1="6" x2="21" y2="6"></line><line x1="3" y1="18" x2="21" y2="18"></line></svg></a>

            <div class="nav-logo align-self-center">
                <a class="navbar-brand" href="{{ route('admin') }}"><img alt="logo" src="{{ asset('assets/img/logo-white.png') }}"></a>
            </div>
            
            <ul class="navbar-item flex-row nav-dropdowns ml-auto">
                <li class="nav-item dropdown user-profile-dropdown order-lg-0 order-1">
                    <a href="javascript:void(0);" class="nav-link dropdown-toggle user" id="user-profile-dropdown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <div class="media">
                            <div class="media-body align-self-center bg-primary py-2 px-3 rounded">
                                <h6>{{ \Auth::user()->name }}</h6>
                                <p>{{ \Auth::user()->email }}</p>
                            </div>
                            {{-- <img src="{{ asset('dashboard/assets/img/90x90.jpg') }}" class="img-fluid" alt="admin-profile"> --}}
                            {{-- <span class="badge badge-success"></span> --}}
                        </div>
                    </a>

                    <div class="dropdown-menu position-absolute pt-0" aria-labelledby="userProfileDropdown">
                        <div class="dropdown-item">
                            <a href="#">
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-user"><path d="M20 21v-2a4 4 0 0 0-4-4H8a4 4 0 0 0-4 4v2"></path><circle cx="12" cy="7" r="4"></circle></svg> <span> Profile</span>
                            </a>
                        </div>
                        <div class="dropdown-item">
                            <a href="{{ route('admin.logout') }}">
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-log-out"><path d="M9 21H5a2 2 0 0 1-2-2V5a2 2 0 0 1 2-2h4"></path><polyline points="16 17 21 12 16 7"></polyline><line x1="21" y1="12" x2="9" y2="12"></line></svg> <span>Log Out</span>
                            </a>
                        </div>
                    </div>
                </li>
            </ul>
        </header>
    </div>
    <!--  END NAVBAR  -->

    <!--  BEGIN MAIN CONTAINER  -->
    <div class="main-container" id="container">

        <div class="overlay"></div>
        <div class="search-overlay"></div>

        <!--  BEGIN TOPBAR  -->
        <div class="topbar-nav header navbar" role="banner">
            <nav id="topbar">
                <ul class="navbar-nav theme-brand flex-row  text-center">
                    <li class="nav-item theme-logo">
                        <a href="{{ route('admin') }}">
                            <img src="{{ asset('assets/img/logo-white.png') }}" class="navbar-logo" alt="logo">
                        </a>
                    </li>
                </ul>

                <ul class="list-unstyled menu-categories" id="topAccordion">

                    <li class="menu single-menu @if ($menuActive == 'beranda') active @endif">
                        <a href="{{ route('admin.dashboard') }}" class="dropdown-toggle autodroprown">
                            <div class="">
                                <span>Beranda</span>
                            </div>
                        </a>
                    </li>

                    <li class="menu single-menu @if ($menuActive == 'konten') active @endif">
                        <a href="#konten" data-toggle="collapse" aria-expanded="true" class="dropdown-toggle autodroprown">
                            <div class="">
                                <span>Konten</span>
                            </div>
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-chevron-down"><polyline points="6 9 12 15 18 9"></polyline></svg>
                        </a>
                        <ul class="collapse submenu list-unstyled" id="konten" data-parent="#topAccordion">
                            <li>
                                <a href="{{ route('admin.article') }}"> Artikel </a>
                            </li>
                            <li>
                                <a href="{{ route('admin.categories') }}"> Kategori </a>
                            </li>
                            <li>
                                <a href="{{ route('admin.galleries') }}"> Galeri Foto </a>
                            </li>
                            <li>
                                <a href="{{ route('admin.videos') }}"> Vidio </a>
                            </li>
                        </ul>
                    </li>

                    <li class="menu single-menu @if ($menuActive == 'publikasi') active @endif">
                        <a href="{{ route('admin.publications') }}" class="dropdown-toggle autodroprown">
                            <div class="">
                                <span>Publikasi</span>
                            </div>
                        </a>
                    </li>

                    <li class="menu single-menu @if ($menuActive == 'komponen') active @endif">
                        <a href="#komponen" data-toggle="collapse" aria-expanded="true" class="dropdown-toggle autodroprown">
                            <div class="">
                                <span>Komponen</span>
                            </div>
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-chevron-down"><polyline points="6 9 12 15 18 9"></polyline></svg>
                        </a>
                        <ul class="collapse submenu list-unstyled" id="komponen" data-parent="#topAccordion">
                            <li>
                                <a href="{{ route('admin.home-carousels') }}"> Home Carousel </a>
                            </li>
                            <li>
                                <a href="{{ route('admin.main-videos') }}"> Vidio Utama </a>
                            </li>
                            <li>
                                <a href="{{ route('admin.attachments') }}"> Lampiran </a>
                            </li>
                        </ul>
                    </li>

                    <li class="menu single-menu @if ($menuActive == 'halaman') active @endif">
                        <a href="{{ route('admin.pages') }}" class="dropdown-toggle autodroprown">
                            <div class="">
                                <span>Halaman</span>
                            </div>
                        </a>
                    </li>

                    {{-- <li class="menu single-menu">
                        <a href="#publikasi" class="dropdown-toggle autodroprown">
                            <div class="">
                                <span>Menu</span>
                            </div>
                        </a>
                    </li> --}}

                    <li class="menu single-menu @if ($menuActive == 'mailing') active @endif">
                        <a href="#mailing" data-toggle="collapse" aria-expanded="true" class="dropdown-toggle autodroprown">
                            <div class="">
                                <span>Mailing</span>
                            </div>
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-chevron-down"><polyline points="6 9 12 15 18 9"></polyline></svg>
                        </a>
                        <ul class="collapse submenu list-unstyled" id="mailing" data-parent="#topAccordion">
                            <li>
                                <a href="{{ route('admin.mailing.create') }}"> Blast Email </a>
                            </li>
                            <li>
                                <a href="{{ route('admin.mailing.lists') }}"> Mailing List </a>
                            </li>
                            <li>
                                <a href="{{ route('admin.mailing.history') }}"> Mailing History </a>
                            </li>
                        </ul>
                    </li>

                    <li class="menu single-menu @if ($menuActive == 'donasi') active @endif">
                        <a href="{{ route('admin.donations') }}" class="dropdown-toggle autodroprown">
                            <div class="">
                                <span>Donor</span>
                            </div>
                        </a>
                    </li>
                </ul>
            </nav>
        </div>
        <!--  END TOPBAR  -->
        
        <!--  BEGIN CONTENT AREA  -->
        <div id="content" class="main-content">
            @yield('content')
            <div class="footer-wrapper">
                <div class="footer-section f-section-1">
                    <p class="">Copyright © 2021 <a target="_blank" href="https://transformasi.org">Transformasi</a>, All rights reserved.</p>
                </div>
                <div class="footer-section f-section-2">
                    {{-- <p class="">Coded with <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-heart"><path d="M20.84 4.61a5.5 5.5 0 0 0-7.78 0L12 5.67l-1.06-1.06a5.5 5.5 0 0 0-7.78 7.78l1.06 1.06L12 21.23l7.78-7.78 1.06-1.06a5.5 5.5 0 0 0 0-7.78z"></path></svg></p> --}}
                </div>
            </div>
        </div>
        <!--  END CONTENT AREA  -->

    </div>
    <!-- END MAIN CONTAINER -->

    <!-- BEGIN GLOBAL MANDATORY SCRIPTS -->
    <script src="{{ asset('dashboard/assets/js/libs/jquery-3.1.1.min.js') }}"></script>
    <script src="{{ asset('dashboard/bootstrap/js/popper.min.js') }}"></script>
    <script src="{{ asset('dashboard/bootstrap/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('dashboard/plugins/perfect-scrollbar/perfect-scrollbar.min.js') }}"></script>
    <script src="{{ asset('dashboard/plugins/sweetalerts/sweetalert2.min.js') }}"></script>
    <script src="{{ asset('dashboard/assets/js/app.js') }}"></script>
    <script>
        $(document).ready(function() {
            App.init();
        });
    </script>
    <script src="{{ asset('dashboard/assets/js/custom.js') }}"></script>
    <!-- END GLOBAL MANDATORY SCRIPTS -->

    <!-- BEGIN PAGE LEVEL PLUGINS/CUSTOM SCRIPTS -->
    {{-- <script src="{{ asset('dashboard/plugins/apex/apexcharts.min.js') }}"></script> --}}
    {{-- <script src="{{ asset('dashboard/assets/js/dashboard/dash_2.js') }}"></script> --}}
    <!-- BEGIN PAGE LEVEL PLUGINS/CUSTOM SCRIPTS -->

    @yield('before-body-end')
</body>
</html>