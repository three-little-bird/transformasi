@extends('admin.layouts.dashboard', [
    'menuActive' => 'halaman'
])

@section('content')
    <div class="layout-px-spacing">
        <div class="row">
            <div class="col-xl-12 col-lg-12 col-md-12 col-12 layout-top-spacing layout-spacing">
                <div class="row mb-2">
                    <div class="col-6">
                        <form action="{{ route('admin.pages') }}">
                            <div class="input-group">
                                <input type="hidden" name="sort" value="{{ request('sort') }}">
                                <input type="text" name="search" class="form-control" placeholder="Cari halaman.." style="max-width: 300px" value="{{ request('search') }}">
                                <div class="input-group-append">
                                    <button class="btn btn-primary">Cari</button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="col-6 text-right">
                        <a href="{{ route('admin.pages.add') }}" class="btn btn-primary">Halaman Baru</a>
                    </div>
                </div>
                <div class="widget widget-content-area br-4">
                    <div class="widget-one">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>
                                        @if (substr(request('sort'), 0, 2) == 'id')
                                            @if (substr(request('sort'), 3) == 'asc')
                                                <a href="{{ route('admin.pages', ['search' => request('search'), 'sort' => 'id,desc']) }}" class="text-primary">ID <i class="fas fa-chevron-down"></i></a>
                                            @else
                                                <a href="{{ route('admin.pages', ['search' => request('search'), 'sort' => 'id,asc']) }}" class="text-primary">ID <i class="fas fa-chevron-up"></i></a>
                                            @endif
                                        @else
                                            <a href="{{ route('admin.pages', ['search' => request('search'), 'sort' => 'id,asc']) }}">ID</a>
                                        @endif
                                    </th>
                                    <th>
                                        @if (substr(request('sort'), 0, 5) == 'title')
                                            @if (substr(request('sort'), 6) == 'asc')
                                                <a href="{{ route('admin.pages', ['search' => request('search'), 'sort' => 'title,desc']) }}" class="text-primary">Judul <i class="fas fa-chevron-down"></i></a>
                                            @else
                                                <a href="{{ route('admin.pages', ['search' => request('search'), 'sort' => 'title,asc']) }}" class="text-primary">Judul <i class="fas fa-chevron-up"></i></a>
                                            @endif
                                        @else
                                            <a href="{{ route('admin.pages', ['search' => request('search'), 'sort' => 'title,asc']) }}">Judul</a>
                                        @endif
                                    </th>
                                    <th>
                                        @if (substr(request('sort'), 0, 6) == 'status')
                                            @if (substr(request('sort'), 7) == 'asc')
                                                <a href="{{ route('admin.pages', ['search' => request('search'), 'sort' => 'status,desc']) }}" class="text-primary">Status <i class="fas fa-chevron-down"></i></a>
                                            @else
                                                <a href="{{ route('admin.pages', ['search' => request('search'), 'sort' => 'status,asc']) }}" class="text-primary">Status <i class="fas fa-chevron-up"></i></a>
                                            @endif
                                        @else
                                            <a href="{{ route('admin.pages', ['search' => request('search'), 'sort' => 'status,asc']) }}">Status</a>
                                        @endif
                                    </th>
                                    <th>
                                        @if (substr(request('sort'), 0, 6) == 'author')
                                            @if (substr(request('sort'), 7) == 'asc')
                                                <a href="{{ route('admin.pages', ['search' => request('search'), 'sort' => 'author,desc']) }}" class="text-primary">Author <i class="fas fa-chevron-down"></i></a>
                                            @else
                                                <a href="{{ route('admin.pages', ['search' => request('search'), 'sort' => 'author,asc']) }}" class="text-primary">Author <i class="fas fa-chevron-up"></i></a>
                                            @endif
                                        @else
                                            <a href="{{ route('admin.pages', ['search' => request('search'), 'sort' => 'author,asc']) }}">Author</a>
                                        @endif
                                    </th>
                                    <th>
                                        @if (substr(request('sort'), 0, 4) == 'lang')
                                            @if (substr(request('sort'), 5) == 'asc')
                                                <a href="{{ route('admin.pages', ['search' => request('search'), 'sort' => 'lang,desc']) }}" class="text-primary">Bahasa <i class="fas fa-chevron-down"></i></a>
                                            @else
                                                <a href="{{ route('admin.pages', ['search' => request('search'), 'sort' => 'lang,asc']) }}" class="text-primary">Bahasa <i class="fas fa-chevron-up"></i></a>
                                            @endif
                                        @else
                                            <a href="{{ route('admin.pages', ['search' => request('search'), 'sort' => 'lang,asc']) }}">Bahasa</a>
                                        @endif
                                    </th>
                                    <th>
                                        @if (substr(request('sort'), 0, 10) == 'created_at')
                                            @if (substr(request('sort'), 11) == 'asc')
                                                <a href="{{ route('admin.pages', ['search' => request('search'), 'sort' => 'created_at,desc']) }}" class="text-primary">Tanggal <i class="fas fa-chevron-down"></i></a>
                                            @else
                                                <a href="{{ route('admin.pages', ['search' => request('search'), 'sort' => 'created_at,asc']) }}" class="text-primary">Tanggal <i class="fas fa-chevron-up"></i></a>
                                            @endif
                                        @else
                                            <a href="{{ route('admin.pages', ['search' => request('search'), 'sort' => 'created_at,asc']) }}">Tanggal</a>
                                        @endif
                                    </th>
                                    <th>
                                        @if (substr(request('sort'), 0, 4) == 'hits')
                                            @if (substr(request('sort'), 5) == 'asc')
                                                <a href="{{ route('admin.pages', ['search' => request('search'), 'sort' => 'hits,desc']) }}" class="text-primary">Hits <i class="fas fa-chevron-down"></i></a>
                                            @else
                                                <a href="{{ route('admin.pages', ['search' => request('search'), 'sort' => 'hits,asc']) }}" class="text-primary">Hits <i class="fas fa-chevron-up"></i></a>
                                            @endif
                                        @else
                                            <a href="{{ route('admin.pages', ['search' => request('search'), 'sort' => 'hits,asc']) }}">Hits</a>
                                        @endif
                                    </th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                @if ($pages->count() < 1)
                                    <tr>
                                        <td colspan="8" align="center"><i>Belum ada halaman</i></td>
                                    </tr>
                                @endif

                                @foreach ($pages as $page)
                                    <tr>
                                        <td>{{ $page->id }}</td>
                                        <td>{{ $page->title }}</td>
                                        <td>{{ $page->status() }}</td>
                                        <td>{{ $page->author ? $page->author->name : $page->created_by_id }}</td>
                                        <td>{{ $page->language() }}</td>
                                        <td>{{ $page->created_at->format('d M Y, H:i') }}</td>
                                        <td>{{ $page->hits }}</td>
                                        <td>
                                            <a href="{{ route('home.page', $page->slug) }}" class="btn btn-sm btn-success m-1" target="_blank">Lihat</a>
                                            <a href="{{ route('admin.pages.edit', $page->id) }}" class="btn btn-sm btn-primary m-1">Ubah</a>
                                            <button class="btn btn-sm btn-danger m-1" data-toggle="popconfirm" data-url="{{ route('admin.pages.delete', $page->id) }}">Hapus</button>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                        {{ $pages->links('vendor.pagination.bootstrap-4') }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection