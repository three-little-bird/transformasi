@extends('admin.layouts.dashboard', [
    'menuActive' => 'komponen'
])

@section('content')
    <div class="layout-px-spacing">
        <div class="row">
            <div class="col-xl-12 col-lg-12 col-md-12 col-12 layout-top-spacing layout-spacing">
                <div class="row mb-2">
                    <div class="col-6">
                        <form action="{{ route('admin.attachments') }}">
                            <div class="input-group">
                                <input type="hidden" name="sort" value="{{ request('sort') }}">
                                <input type="text" name="search" class="form-control" placeholder="Cari lampiran.." style="max-width: 300px" value="{{ request('search') }}">
                                <div class="input-group-append">
                                    <button class="btn btn-primary">Cari</button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="col-6 text-right">
                        <a href="{{ route('admin.attachments.add') }}" class="btn btn-primary">Lampiran Baru</a>
                    </div>
                </div>
                <div class="widget widget-content-area br-4">
                    <div class="widget-one">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Judul</th>
                                    <th>Jumlah Unduhan</th>
                                    <th>Bahasa</th>
                                    <th>Tanggal</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                @if ($attachments->count() < 1)
                                    <tr>
                                        <td colspan="6" align="center">Belum ada lampiran</td>
                                    </tr>
                                @endif

                                @foreach ($attachments as $attachment)
                                    <tr>
                                        <td>{{ $attachment->id }}</td>
                                        <td>{{ $attachment->title }}</td>
                                        <td>{{ $attachment->download_count }}</td>
                                        <td>{{ $attachment->language() }}</td>
                                        <td>{{ $attachment->created_at->format('d M Y, H:i') }}</td>
                                        <td>
                                            <a href="{{ route('admin.attachments.edit', $attachment->id) }}" class="btn btn-sm btn-primary m-1">Ubah</a>
                                            <button class="btn btn-sm btn-danger m-1" data-toggle="popconfirm" data-url="{{ route('admin.attachments.delete', $attachment->id) }}">Hapus</button>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                        {{ $attachments->links('vendor.pagination.bootstrap-4') }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
