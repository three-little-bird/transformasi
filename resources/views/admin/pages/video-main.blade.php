@extends('admin.layouts.dashboard', [
    'menuActive' => 'komponen'
])

@section('before-body-end')
    <link rel="stylesheet" href="{{ asset('dashboard/plugins/bootstrap-select/bootstrap-select.min.css') }}">
    <script src="{{ asset('dashboard/plugins/bootstrap-select/bootstrap-select.min.js') }}"></script>
@endsection

@section('content')
    <div class="layout-px-spacing">
        <div class="row">
            <div class="col-xl-6 col-lg-6 col-md-6 col-12 layout-top-spacing layout-spacing">
                <div class="widget widget-content-area br-4">
                    <div class="widget-one">
                        <h5>Indonesia</h5>
                        <form action="{{ route('admin.main-videos.update', 'id') }}" method="POST">
                            @csrf
                            <div class="form-group">
                                <label>Vidio Utama</label>
                                <select name="video_id" class="form-control selectpicker" data-live-search="true">
                                    <option value="">-- Tidak satupun --</option>
                                    @foreach ($videosId as $video)
                                    <option value="{{ $video->id }}" data-tokens="{{ $video->title }}" @if($video->id == @$videoId->id) selected @endif>{{ \Str::limit($video->title, 150) }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="text-center">
                                <button class="btn btn-primary">Simpan</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            
            <div class="col-xl-6 col-lg-6 col-md-6 col-12 layout-top-spacing layout-spacing">
                <div class="widget widget-content-area br-4">
                    <div class="widget-one">
                        <h5>English</h5>
                        <form action="{{ route('admin.main-videos.update', 'en') }}" method="POST">
                            @csrf
                            <div class="form-group">
                                <label>Vidio Utama</label>
                                <select name="video_id" class="form-control selectpicker" data-live-search="true">
                                    <option value="">-- Tidak satupun --</option>
                                    @foreach ($videosEn as $video)
                                        <option value="{{ $video->id }}" data-tokens="{{ $video->title }}" @if($video->id == @$videoEn->id) selected @endif>{{ \Str::limit($video->title, 150) }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="text-center">
                                <button class="btn btn-primary">Simpan</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
