@extends('admin.layouts.dashboard', [
    'menuActive' => 'beranda'
])

@section('content')
    <div class="layout-px-spacing">
        <div class="row">
            <div class="col-xl-12 col-lg-12 col-md-12 col-12 layout-top-spacing layout-spacing">
                <div class="widget widget-content-area br-4">
                    <div class="widget-one">

                        <h6>Welcome, {{ \Auth::user()->name }}</h6>

                        <p class="mb-0 mt-3">Lorem ipsum</p>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection