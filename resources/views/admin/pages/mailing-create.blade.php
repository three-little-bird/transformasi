@extends('admin.layouts.dashboard', [
    'menuActive' => 'mailing'
])

@section('before-body-end')
    <link rel="stylesheet" href="{{ asset('dashboard/assets/css/apps/mailbox.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('dashboard/plugins/editors/quill/quill.snow.css') }}">
    <script src="{{ asset('dashboard/plugins/editors/quill/quill.js') }}"></script>
    <script>
        var quill = new Quill('#editor-container', {
            modules: {
                toolbar: [
                [{ header: [1, 2, false] }],
                ['bold', 'italic', 'underline'],
                // ['image']
                ]
            },
            placeholder: 'Isi blast...',
            name: 'content',
            theme: 'snow'  // or 'bubble'
        });
        $("#form").on("submit",function(){
            $("#hiddenArea").val($("#editor-container .ql-editor").html());
        });

        let addRecipient = function() {
            $("#recipient_box").append(`
                <div class="row">
                    <div class="col-md-3">
                        <div class="d-flex my-2 mail-to">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-user"><path d="M20 21v-2a4 4 0 0 0-4-4H8a4 4 0 0 0-4 4v2"></path><circle cx="12" cy="7" r="4"></circle></svg>
                            <div class="">
                                <select name="recipient_type[]" class="type-input form-control">
                                    <option value="email">Ke Email</option>
                                    <option value="list">Ke Mailing List</option>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-7">
                        <div class="my-2">
                            <div class="value-input-wrapper">
                                
                            </div>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <a href="javascript:void(0);" class="delete-item d-inline-block mt-3" data-toggle="tooltip" data-placement="top" title="" data-original-title="Delete"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x-circle"><circle cx="12" cy="12" r="10"></circle><line x1="15" y1="9" x2="9" y2="15"></line><line x1="9" y1="9" x2="15" y2="15"></line></svg></a>
                    </div>
                </div>
            `);

            $("#recipient_box > .row")
                .last()
                .find('.type-input')
                .change();
        }

        $("#recipient_box").on("change", ".type-input", function() {
            let type = $(this).val();
            let row = $(this).closest(".row");

            if (type == 'email') {
                row.find('.value-input-wrapper')
                    .html(`
                        <input type="text" name="recipient_value[]" placeholder="Email.." class="form-control">
                    `);
            } else {
                row.find('.value-input-wrapper')
                    .html(`
                        <select name="recipient_value[]" class="form-control">
                            @foreach ($mailings as $mailing)
                                <option value="{{ $mailing->id }}">{{ $mailing->name }} ({{ $mailing->mailing_lists_count }} email)</option>
                            @endforeach
                        </select>
                    `);
            }
        });

        $("#recipient_box").on("click", ".delete-item", function() {
            let row = $(this).closest(".row");
            let siblingCount = row.siblings().length;
            row.remove();

            if (siblingCount == 0) {
                addRecipient();
            }
        });

        addRecipient();
    </script>
@endsection

@section('content')
    <div class="layout-px-spacing">

        <h3 class="pt-5">Blast Email</h3>
        <form id="form" action="{{ route('admin.mailing.blast') }}" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="row">
                <div class="col-xl-9 col-lg-9 col-md-9 col-12 layout-top-spacing layout-spacing">
                    <div class="widget widget-content-area compose-box br-4">
                        <div class="widget-one compose-content">
                            <div id="recipient_box">

                            </div>
                            <div class="row">
                                <div class="col-md-10 text-right">
                                    <button type="button" onclick="addRecipient()" class="btn btn-sm btn-primary">Tambah Penerima</button>
                                </div>
                            </div>

                            <div class="d-flex my-4 mail-subject">
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-mail"><path d="M4 4h16c1.1 0 2 .9 2 2v12c0 1.1-.9 2-2 2H4c-1.1 0-2-.9-2-2V6c0-1.1.9-2 2-2z"></path><polyline points="22,6 12,13 2,6"></polyline></svg>
                                <div class="w-100">
                                    <input type="text" name="subject" id="m-subject" placeholder="Subjek" class="form-control">
                                </div>
                            </div>

                            <div id="editor-container">

                            </div>
                            <textarea name="content" style="display:none" id="hiddenArea"></textarea>

                            <div class="d-flex">
                                <input type="file" name="attachments[]" class="form-control-file my-4" id="mail_File_attachment" multiple="multiple">
                            </div>
                            <div class="text-right mt-3">
                                <button class="btn btn-primary">Kirim</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection