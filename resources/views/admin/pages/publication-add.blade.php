@extends('admin.layouts.dashboard', [
    'menuActive' => 'publikasi'
])

@section('before-body-end')
    <link rel="stylesheet" href="{{ asset('dashboard/plugins/summernote/summernote-bs4.min.css') }}">
    <link rel="stylesheet" href="{{ asset('dashboard/plugins/flatpickr/flatpickr.css') }}">
    <link rel="stylesheet" href="{{ asset('dashboard/plugins/bootstrap-select/bootstrap-select.min.css') }}">
    <script src="{{ asset('dashboard/plugins/summernote/summernote-bs4.min.js') }}"></script>
    <script src="{{ asset('dashboard/plugins/flatpickr/flatpickr.js') }}"></script>
    <script src="{{ asset('dashboard/plugins/bootstrap-select/bootstrap-select.min.js') }}"></script>
    <script>
        $('.summernote').summernote();
        $('div.note-editable').height(250);
        flatpickr(document.querySelector('.published-input'));

        let updateType = function () {
            let type = $(".type-input").val();

            // Topic
            if (type === 'policy-brief') {
                $(".topic-col").show();
                $(".published-col").hide()
                    .find('input').removeAttr('required', true);
            } else {
                $(".topic-col").hide();
                $(".published-col").show()
                    .find('input').attr('required', true);
            }

            // Author
            if (type === 'book') {
                $(".author-col").show()
                    .find('input').attr('required', true);
            } else {
                $(".author-col").hide()
                    .find('input').removeAttr('required');
            }
        }
        updateType();

        $('form').on('change', function(){
            console.log('updated')
            updateType();
        });
    </script>
@endsection

@section('content')
    <div class="layout-px-spacing">

        <h3 class="pt-5">Publikasi Baru</h3>
        @include('admin.components.error-message')
        <form action="{{ route('admin.publications.add') }}" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="row">
                <div class="col-xl-9 col-lg-9 col-md-9 col-12 layout-top-spacing layout-spacing">
                    <div class="widget widget-content-area br-4">
                        <div class="widget-one">
                            <ul class="nav nav-tabs  mb-3 mt-3" id="simpletab" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active" id="content-tab" data-toggle="tab" href="#content-pane" role="tab" aria-controls="content" aria-selected="true">Content</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="attachment-tab" data-toggle="tab" href="#attachment" role="tab" aria-controls="attachment" aria-selected="true">Attachments</a>
                                </li>
                            </ul>
                            <div class="tab-content" id="simpletabContent">
                                <div class="tab-pane fade show active" id="content-pane">
                                    <div class="row">
                                        <div class="col-md-8">
                                            <div class="form-group">
                                                <label>Judul</label>
                                                <input type="text" class="form-control" name="title" value="" autocomplete="off" placeholder="Judul publikasi.." required>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Tipe</label>
                                                <select name="type" class="form-control type-input">
                                                    @foreach ($types as $type)
                                                        <option value="{{ $type }}">{{ ucfirst($type) }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-8">
                                            <div class="form-group">
                                                <label>Foto</label>
                                                <input type="file" class="form-control-file" name="image" value="" autocomplete="off" placeholder="Berkas foto.." accept=".jpg,.jpeg,.png" required>
                                            </div>
                                        </div>
                                        <div class="col-md-4 topic-col">
                                            <div class="form-group">
                                                <label>Topik</label>
                                                <select name="topic_id" class="form-control">
                                                    @foreach ($topics as $topic)
                                                        <option value="{{ $topic->id }}">{{ $topic->name }} ({{ $topic->language() }})</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-4 published-col">
                                            <div class="form-group">
                                                <label>Tanggal Publikasi</label>
                                                <input type="text" name="publish_date" value="{{ now()->format('Y-m-d') }}" class="form-control published-input" required>
                                            </div>
                                        </div>
                                        <div class="col-md-12 author-col">
                                            <div class="form-group">
                                                <label>Author</label>
                                                <input type="text" class="form-control" name="author" value="" autocomplete="off" placeholder="Author.." required>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label>Deskripsi</label>
                                                <textarea name="description" class="summernote" rows="10"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="attachment">
                                    @for ($i = 1; $i <= 15; $i++)
                                        <div class="form-group">
                                            <label>Attachment {{ $i }}</label>
                                            <select name="attachment[]" class="selectpicker form-control" data-live-search="true">
                                                <option value="">-- Select attachment --</option>
                                                @foreach ($attachments as $attachment)
                                                    <option value="{{ $attachment->id }}">{{ $attachment->title }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    @endfor
                                </div>
                            </div>
                            <div class="text-right mt-3">
                                <button class="btn btn-primary">Simpan</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            
            <div class="col-xl-3 col-lg-3 col-md-3 col-12 layout-top-spacing layout-spacing">
                <div class="widget widget-content-area br-4">
                    <div class="widget-one">
                        <div class="form-group">
                            <label>Status</label>
                            <select name="is_published" class="form-control">
                                <option value="1">Published</option>
                                <option value="0">Unpublished</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Bahasa</label>
                            <select name="lang" class="form-control">
                                <option value="*">Semua</option>
                                <option value="id-ID">Indonesia</option>
                                <option value="en-GB">English</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection