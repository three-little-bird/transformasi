@extends('admin.layouts.dashboard', [
    'menuActive' => 'konten'
])

@section('before-body-end')
    <link rel="stylesheet" href="{{ asset('dashboard/plugins/bootstrap-select/bootstrap-select.min.css') }}">
    <link rel="stylesheet" href="{{ asset('dashboard/plugins/summernote/summernote-bs4.min.css') }}">
    <script src="{{ asset('dashboard/plugins/bootstrap-select/bootstrap-select.min.js') }}"></script>
    <script src="{{ asset('dashboard/plugins/summernote/summernote-bs4.min.js') }}"></script>
    <script>
        $('.summernote').summernote();
        $('div.note-editable').height(250);
    </script>
@endsection

@section('content')
    <div class="layout-px-spacing">

        <h3 class="pt-5">Edit Album</h3>
        <form action="{{ route('admin.galleries.edit', $gallery->id) }}" method="POST">
            @csrf
            <div class="row">
                <div class="col-xl-9 col-lg-9 col-md-9 col-12 layout-top-spacing layout-spacing">
                    <div class="widget widget-content-area br-4">
                        <div class="widget-one">
                            <div class="row">
                                <div class="col-md-8">
                                    <div class="form-group">
                                        <label>Judul</label>
                                        <input type="text" class="form-control" name="title" value="{{ $gallery->title }}" autocomplete="off" placeholder="Judul vidio.." required>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Description</label>
                                        <textarea name="description" class="summernote" rows="10">{{ $gallery->description }}</textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="text-right mt-3">
                                <button class="btn btn-primary">Simpan</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            
            <div class="col-xl-3 col-lg-3 col-md-3 col-12 layout-top-spacing layout-spacing">
                <div class="widget widget-content-area br-4">
                    <div class="widget-one">
                        <div class="form-group">
                            <label>Status</label>
                            <select name="is_published" class="selectpicker w-100" data-live-search="true">
                                <option value="1" @if($gallery->state == 1) selected @endif>Published</option>
                                <option value="0" @if($gallery->state != 1) selected @endif>Unpublished</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Bahasa</label>
                            <select name="lang" class="form-control">
                                <option value="*" @if($gallery->lang == '*') selected @endif>Semua</option>
                                <option value="id-ID" @if($gallery->lang == 'id-ID') selected @endif>Indonesia</option>
                                <option value="en-GB" @if($gallery->lang == 'en-GB') selected @endif>English</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection