@extends('admin.layouts.dashboard', [
    'menuActive' => 'konten'
])

@section('content')
    <div class="layout-px-spacing">

        <h3 class="pt-5">Edit Carousel</h3>
        <form action="{{ route('admin.home-carousels.edit', $carousel->id) }}" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="row">
                <div class="col-xl-9 col-lg-9 col-md-9 col-12 layout-top-spacing layout-spacing">
                    <div class="widget widget-content-area br-4">
                        <div class="widget-one">
                            <div class="row">
                                <div class="col-md-8">
                                    <div class="form-group">
                                        <label>Judul</label>
                                        <input type="text" class="form-control" name="title" value="{{ $carousel->title }}" autocomplete="off" placeholder="Judul..">
                                    </div>
                                </div>
                                <div class="col-md-8">
                                    <div class="form-group">
                                        <label>Background</label>
                                        <input type="file" name="file" class="form-control-file" accept=".jpg,.jpeg,.png">
                                    </div>
                                </div>
                                <div class="col-md-8">
                                    <div class="form-group">
                                        <label>Link</label>
                                        <input type="text" class="form-control" name="link" value="{{ $carousel->link }}" autocomplete="off" placeholder="Link (call to action)..">
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Description</label>
                                        <textarea name="description" class="form-control" rows="5">{{ $carousel->description }}</textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="text-right mt-3">
                                <button class="btn btn-primary">Simpan</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            
            <div class="col-xl-3 col-lg-3 col-md-3 col-12 layout-top-spacing layout-spacing">
                <div class="widget widget-content-area br-4">
                    <div class="widget-one">
                        <div class="form-group">
                            <label>Status</label>
                            <select name="is_published" class="form-control">
                                <option value="1" @if($carousel->is_published) selected @endif>Published</option>
                                <option value="0" @if(!$carousel->is_published) selected @endif>Unpublished</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Bahasa</label>
                            <select name="lang" class="form-control">
                                <option value="*" @if($carousel->lang == '*') selected @endif>Semua</option>
                                <option value="id-ID" @if($carousel->lang == 'id-ID') selected @endif>Indonesia</option>
                                <option value="en-GB" @if($carousel->lang == 'en-GB') selected @endif>English</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection