@extends('admin.layouts.dashboard', [
    'menuActive' => 'konten'
])

@section('content')
    <div class="layout-px-spacing">
        <div class="row">
            <div class="col-xl-12 col-lg-12 col-md-12 col-12 layout-top-spacing layout-spacing">
                <div class="row mb-2">
                    <div class="col-6">
                        <form action="{{ route('admin.videos') }}">
                            <div class="input-group">
                                <input type="hidden" name="sort" value="{{ request('sort') }}">
                                <input type="text" name="search" class="form-control" placeholder="Cari vidio.." style="max-width: 300px" value="{{ request('search') }}">
                                <div class="input-group-append">
                                    <button class="btn btn-primary">Cari</button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="col-6 text-right">
                        <a href="{{ route('admin.videos.add') }}" class="btn btn-primary">Vidio Baru</a>
                    </div>
                </div>
                <div class="widget widget-content-area br-4">
                    <div class="widget-one">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Judul</th>
                                    <th>Bahasa</th>
                                    <th>Tanggal</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                @if ($videos->count() < 1)
                                    <tr>
                                        <td colspan="5" align="center">Belum ada vidio</td>
                                    </tr>
                                @endif

                                @foreach ($videos as $video)
                                    <tr>
                                        <td>{{ $video->id }}</td>
                                        <td>{{ $video->title }}</td>
                                        <td>{{ $video->language() }}</td>
                                        <td>{{ $video->created_at->format('d M Y, H:i') }}</td>
                                        <td>
                                            <a href="{{ route('admin.videos.edit', $video->id) }}" class="btn btn-sm btn-primary m-1">Ubah</a>
                                            <button class="btn btn-sm btn-danger m-1" data-toggle="popconfirm" data-url="{{ route('admin.videos.delete', $video->id) }}">Hapus</button>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                        {{ $videos->links('vendor.pagination.bootstrap-4') }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
