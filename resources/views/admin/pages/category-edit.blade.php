@extends('admin.layouts.dashboard', [
    'menuActive' => 'konten'
])

@section('before-body-end')
    <link rel="stylesheet" href="{{ asset('dashboard/plugins/bootstrap-select/bootstrap-select.min.css') }}">
    <script src="{{ asset('dashboard/plugins/bootstrap-select/bootstrap-select.min.js') }}"></script>
@endsection

@section('content')
    <div class="layout-px-spacing">

        <h3 class="pt-5">Edit Kategori</h3>
        <form action="{{ route('admin.categories.edit', $category->id) }}" method="POST">
            @csrf
            <div class="row">
                <div class="col-xl-9 col-lg-9 col-md-9 col-12 layout-top-spacing layout-spacing">
                    <div class="widget widget-content-area br-4">
                        <div class="widget-one">
                            <div class="row">
                                <div class="col-md-8">
                                    <div class="form-group">
                                        <label>Nama Kategori</label>
                                        <input type="text" class="form-control" name="name" value="{{ $category->name }}" autocomplete="off" placeholder="Nama kategori.." required>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label>Alias</label>
                                        <input type="text" class="form-control" name="slug" value="{{ $category->slug }}" autocomplete="off" placeholder="Otomatis..">
                                    </div>
                                </div>
                            </div>
                            <div class="text-right mt-3">
                                <button class="btn btn-primary">Simpan</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            
            <div class="col-xl-3 col-lg-3 col-md-3 col-12 layout-top-spacing layout-spacing">
                <div class="widget widget-content-area br-4">
                    <div class="widget-one">
                        <div class="form-group">
                            <label>Parent</label>
                            <select name="parent_id" class="selectpicker w-100" data-live-search="true">
                                @foreach ($categories as $cat)
                                    <option value="{{ $cat->id }}" @if($cat->id == $category->parent_id) selected @endif>{{ $cat->name }} ({{ substr($cat->lang, 0, 2) }})</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Bahasa</label>
                            <select name="lang" class="form-control">
                                <option value="*" @if($category->lang == '*') selected @endif>Semua</option>
                                <option value="id-ID" @if($category->lang == 'id-ID') selected @endif>Indonesia</option>
                                <option value="en-GB" @if($category->lang == 'en-GB') selected @endif>English</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection