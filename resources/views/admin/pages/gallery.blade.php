@extends('admin.layouts.dashboard', [
'menuActive' => 'konten'
])

@section('content')
    <div class="layout-px-spacing">
        <div class="row">
            <div class="col-xl-12 col-lg-12 col-md-12 col-12 layout-top-spacing layout-spacing">
                <ul class="nav nav-tabs">
                    <li class="nav-item">
                        <a class="nav-link active" aria-current="page" href="{{ route('admin.galleries') }}">Album</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('admin.galleries-photo') }}">Foto</a>
                    </li>
                </ul>
                <div class="widget widget-content-area br-4">
                    <div class="widget-one">
                        <div class="text-right mb-3">
                            <a href="{{ route('admin.galleries.add') }}" class="btn btn-primary">Album Baru</a>
                        </div>
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Nama</th>
                                    <th>Jumlah Foto</th>
                                    <th>Bahasa</th>
                                    <th>Tanggal</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                @if ($galleries->count() < 1)
                                    <tr>
                                        <td colspan="6" align="center">Belum ada album</td>
                                    </tr>
                                @endif
                                @foreach ($galleries as $gallery)
                                    <tr>
                                        <td>{{ $gallery->id }}</td>
                                        <td>{{ $gallery->title }}</td>
                                        <td>{{ $gallery->photos->count() }}</td>
                                        <td>{{ $gallery->language() }}</td>
                                        <td>{{ $gallery->created_at->format('d F Y, H:i') }}</td>
                                        <td>
                                            <a href="{{ route('admin.galleries.edit', $gallery->id) }}" class="btn btn-sm btn-primary m-1">Ubah</a>
                                            <button class="btn btn-sm btn-danger m-1" data-toggle="popconfirm" data-url="{{ route('admin.galleries.delete', $gallery->id) }}">Hapus</button>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                        {{ $galleries->links('vendor.pagination.bootstrap-4') }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
