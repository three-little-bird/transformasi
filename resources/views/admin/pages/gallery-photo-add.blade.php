@extends('admin.layouts.dashboard', [
    'menuActive' => 'konten'
])

@section('before-body-end')
    <link rel="stylesheet" href="{{ asset('dashboard/plugins/bootstrap-select/bootstrap-select.min.css') }}">
    <link rel="stylesheet" href="{{ asset('dashboard/plugins/summernote/summernote-bs4.min.css') }}">
    <script src="{{ asset('dashboard/plugins/bootstrap-select/bootstrap-select.min.js') }}"></script>
    <script src="{{ asset('dashboard/plugins/summernote/summernote-bs4.min.js') }}"></script>
    <script>
        $('.summernote').summernote();
        $('div.note-editable').height(250);
    </script>
@endsection

@section('content')
    <div class="layout-px-spacing">

        <h3 class="pt-5">Foto Baru</h3>
        <form action="{{ route('admin.galleries-photo.add') }}" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="row">
                <div class="col-xl-9 col-lg-9 col-md-9 col-12 layout-top-spacing layout-spacing">
                    <div class="widget widget-content-area br-4">
                        <div class="widget-one">
                            <div class="row">
                                <div class="col-md-8">
                                    <div class="form-group">
                                        <label>Judul</label>
                                        <input type="text" class="form-control" name="title" value="" autocomplete="off" placeholder="Judul foto.." required>
                                    </div>
                                </div>
                                <div class="col-md-8">
                                    <div class="form-group">
                                        <label>Album</label>
                                        <select name="gallery_id" class="selectpicker w-100" data-live-search="true" required>
                                            @foreach ($galleries as $gallery)
                                                <option value="{{ $gallery->id }}">{{ $gallery->title }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-8">
                                    <div class="form-group">
                                        <label>Foto</label>
                                        <input type="file" class="form-control" name="image" value="" autocomplete="off" placeholder="Berkas foto.." required>
                                    </div>
                                </div>
                                <div class="col-md-8">
                                    <div class="form-group">
                                        <label>Alt</label>
                                        <input type="text" class="form-control" name="alt" value="" autocomplete="off" placeholder="Alternative Text.. (optional)">
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Description</label>
                                        <textarea name="description" class="summernote" rows="10"></textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="text-right mt-3">
                                <button class="btn btn-primary">Simpan</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            
            <div class="col-xl-3 col-lg-3 col-md-3 col-12 layout-top-spacing layout-spacing">
                <div class="widget widget-content-area br-4">
                    <div class="widget-one">
                        <div class="form-group">
                            <label>Bahasa</label>
                            <select name="lang" class="form-control">
                                <option value="*">Semua</option>
                                <option value="id-ID">Indonesia</option>
                                <option value="en-GB">English</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection