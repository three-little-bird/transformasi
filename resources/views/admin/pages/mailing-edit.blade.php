@extends('admin.layouts.dashboard', [
    'menuActive' => 'mailing'
])

@section('before-body-end')
    <link rel="stylesheet" href="{{ asset('dashboard/plugins/bootstrap-select/bootstrap-select.min.css') }}">
    <script src="{{ asset('dashboard/plugins/bootstrap-select/bootstrap-select.min.js') }}"></script>
@endsection

@section('content')
    <div class="layout-px-spacing">

        <h3 class="pt-5">Edit Mailing List</h3>
        <form action="{{ route('admin.mailing.lists.edit', $mailing->id) }}" method="POST">
            @csrf
            <div class="row">
                <div class="col-xl-9 col-lg-9 col-md-9 col-12 layout-top-spacing layout-spacing">
                    <div class="widget widget-content-area br-4">
                        <div class="widget-one">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Nama List</label>
                                        <input type="text" class="form-control" name="name" value="{{ $mailing->name }}" autocomplete="off" placeholder="Nama list.." required>
                                    </div>
                                </div>
                            </div>
                            <div class="text-right mt-3">
                                <button class="btn btn-primary">Simpan</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection