@extends('admin.layouts.dashboard', [
    'menuActive' => 'konten'
])

@section('before-body-end')
    <link rel="stylesheet" href="{{ asset('dashboard/plugins/bootstrap-select/bootstrap-select.min.css') }}">
    <link rel="stylesheet" href="{{ asset('dashboard/plugins/summernote/summernote-bs4.min.css') }}">
    <script src="{{ asset('dashboard/plugins/bootstrap-select/bootstrap-select.min.js') }}"></script>
    <script src="{{ asset('dashboard/plugins/summernote/summernote-bs4.min.js') }}"></script>
    <script>
        $('.summernote').summernote();
        $('div.note-editable').height(250);
    </script>
@endsection

@section('content')
    <div class="layout-px-spacing">
        @include('admin.components.error-message')
        <form action="{{ route('admin.article.edit', $article->id) }}" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="row">
                <div class="col-xl-9 col-lg-9 col-md-9 col-12 layout-top-spacing layout-spacing">
                    <div class="widget widget-content-area br-4">
                        <div class="widget-one">
                            <ul class="nav nav-tabs  mb-3 mt-3" id="simpletab" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active" id="content-tab" data-toggle="tab" href="#content-pane" role="tab" aria-controls="content" aria-selected="true">Content</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="seo-tab" data-toggle="tab" href="#seo" role="tab" aria-controls="seo" aria-selected="true">SEO</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="attachment-tab" data-toggle="tab" href="#attachment" role="tab" aria-controls="attachment" aria-selected="true">Attachments</a>
                                </li>
                            </ul>
                            <div class="tab-content" id="simpletabContent">
                                <div class="tab-pane fade show active" id="content-pane">
                                    <div class="row">
                                        <div class="col-md-8">
                                            <div class="form-group">
                                                <label>Judul</label>
                                                <input type="text" class="form-control" name="title" value="{{ $article->title }}" autocomplete="off" placeholder="Judul artikel" required>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Alias</label>
                                                <input type="text" class="form-control" name="slug" value="{{ $article->slug }}" autocomplete="off" placeholder="Otomatis..">
                                            </div>
                                        </div>
                                        <div class="col-md-8">
                                            <div class="form-group">
                                                <label>Image Intro</label>
                                                <input type="file" class="form-control-file" name="image" value="" autocomplete="off" placeholder="Berkas.." accept=".png,.jpg,.jpeg">
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label>Kategori</label>
                                                <select name="category_id" class="selectpicker w-100" data-live-search="true" required>
                                                    @foreach ($categories as $category)
                                                        <option value="{{ $category->id }}" @if($category->id == $article->category_id) selected @endif>{{ $category->name }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <textarea name="content" class="summernote" rows="10">{!! $article->content !!}</textarea>
                                </div>
                                <div class="tab-pane fade" id="seo">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Author</label>
                                                <input type="text" class="form-control" name="metadata[author]" value="{{ @$article->meta()['author'] }}" autocomplete="off" placeholder="Meta Author">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Key</label>
                                                <input type="text" class="form-control" name="metadata[key]" value="{{ @$article->meta()['key'] }}" autocomplete="off" placeholder="Meta Key">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Description</label>
                                                <textarea name="metadata[description]" class="form-control" rows="4" placeholder="Meta Description">{{ @$article->meta()['description'] }}</textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Robots</label>
                                                <input type="text" class="form-control" name="metadata[robots]" value="{{ @$article->meta()['robots'] }}" autocomplete="off" placeholder="Meta Robots">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="attachment">
                                    @for ($i = 1; $i <= 15; $i++)
                                        <div class="form-group">
                                            <label>Attachment {{ $i }}</label>
                                            <select name="attachment[]" class="selectpicker form-control" data-live-search="true">
                                                <option value="">-- Select attachment --</option>
                                                @foreach ($attachments as $attachment)
                                                    <option value="{{ $attachment->id }}" @if(isset($article->getAttachmentIds()[$i - 1]) && $attachment->id == $article->getAttachmentIds()[$i - 1]) selected @endif>{{ $attachment->title }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    @endfor
                                </div>
                            </div>
                            <div class="text-right mt-3">
                                <button class="btn btn-primary">Simpan</button>
                            </div>
                        </div>
                    </div>
                </div>
            
                <div class="col-xl-3 col-lg-3 col-md-3 col-12 layout-top-spacing layout-spacing">
                    <div class="widget widget-content-area br-4">
                        <div class="widget-one">
                            <div class="form-group">
                                <label>Bahasa</label>
                                <select name="lang" class="form-control">
                                    <option value="*" @if($article->lang == '*') selected @endif>Semua</option>
                                    <option value="id-ID" @if($article->lang == 'id-ID') selected @endif>Indonesia</option>
                                    <option value="en-GB" @if($article->lang == 'en-GB') selected @endif>English</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Status</label>
                                <select name="state" class="form-control">
                                    <option value="1" @if($article->state == 1) selected @endif>Published</option>
                                    <option value="0" @if($article->state == 0) selected @endif>Draft</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
@endsection