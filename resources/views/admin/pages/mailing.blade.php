@extends('admin.layouts.dashboard', [
    'menuActive' => 'mailing'
])

@section('content')
    <div class="layout-px-spacing">
        <div class="row">
            <div class="col-xl-12 col-lg-12 col-md-12 col-12 layout-top-spacing layout-spacing">
                <div class="row mb-2">
                    <div class="col-6">
                        <form action="{{ route('admin.mailing.lists') }}">
                            <div class="input-group">
                                <input type="hidden" name="sort" value="{{ request('sort') }}">
                                <input type="text" name="search" class="form-control" placeholder="Cari mailing list.." style="max-width: 300px" value="{{ request('search') }}">
                                <div class="input-group-append">
                                    <button class="btn btn-primary">Cari</button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="col-6 text-right">
                        <a href="{{ route('admin.mailing.lists.add') }}" class="btn btn-primary">Mailing List Baru</a>
                    </div>
                </div>
                <div class="widget widget-content-area br-4">
                    <div class="widget-one">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Nama</th>
                                    <th>Jumlah Email</th>
                                    <th>Tanggal</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                @if ($mailings->count() < 1)
                                    <tr>
                                        <td colspan="5" align="center">Belum ada list</td>
                                    </tr>
                                @endif

                                @foreach ($mailings as $mailing)
                                    <tr>
                                        <td>{{ $mailing->id }}</td>
                                        <td>{{ $mailing->name }}</td>
                                        <td>{{ $mailing->mailing_lists_count }}</td>
                                        <td>{{ $mailing->created_at->format('d M Y, H:i') }}</td>
                                        <td>
                                            <a href="{{ route('admin.mailing.lists.edit', $mailing->id) }}" class="btn btn-sm btn-primary m-1">Ubah</a>
                                            <button class="btn btn-sm btn-danger m-1" data-toggle="popconfirm" data-url="{{ route('admin.mailing.lists.delete', $mailing->id) }}">Hapus</button>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                        {{ $mailings->links('vendor.pagination.bootstrap-4') }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
