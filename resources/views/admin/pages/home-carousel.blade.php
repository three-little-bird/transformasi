@extends('admin.layouts.dashboard', [
    'menuActive' => 'komponen'
])

@section('content')
    <div class="layout-px-spacing">
        <div class="row">
            <div class="col-xl-12 col-lg-12 col-md-12 col-12 layout-top-spacing layout-spacing">
                <div class="row mb-2">
                    <div class="col-6">
                        <form action="{{ route('admin.home-carousels') }}">
                            <div class="input-group">
                                <input type="hidden" name="sort" value="{{ request('sort') }}">
                                <input type="text" name="search" class="form-control" placeholder="Cari carousel.." style="max-width: 300px" value="{{ request('search') }}">
                                <div class="input-group-append">
                                    <button class="btn btn-primary">Cari</button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="col-6 text-right">
                        <a href="{{ route('admin.home-carousels.add') }}" class="btn btn-primary">Carousel Baru</a>
                    </div>
                </div>
                <div class="widget widget-content-area br-4">
                    <div class="widget-one">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Judul</th>
                                    <th>Bahasa</th>
                                    <th>Tanggal</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                @if ($carousels->count() < 1)
                                    <tr>
                                        <td colspan="5" align="center">Belum ada carousel</td>
                                    </tr>
                                @endif

                                @foreach ($carousels as $carousel)
                                    <tr>
                                        <td>{{ $carousel->id }}</td>
                                        <td>{{ $carousel->title }}</td>
                                        <td>{{ $carousel->language() }}</td>
                                        <td>{{ $carousel->created_at->format('d M Y, H:i') }}</td>
                                        <td>
                                            <a href="{{ route('admin.home-carousels.edit', $carousel->id) }}" class="btn btn-sm btn-primary m-1">Ubah</a>
                                            <button class="btn btn-sm btn-danger m-1" data-toggle="popconfirm" data-url="{{ route('admin.home-carousels.delete', $carousel->id) }}">Hapus</button>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                        {{ $carousels->links('vendor.pagination.bootstrap-4') }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
