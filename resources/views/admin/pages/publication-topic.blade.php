@extends('admin.layouts.dashboard', [
    'menuActive' => 'publikasi'
])

@section('content')
    <div class="layout-px-spacing">
        <div class="row">
            <div class="col-xl-12 col-lg-12 col-md-12 col-12 layout-top-spacing layout-spacing">
                <div class="row mb-2">
                    <div class="col-6">
                        <form action="{{ route('admin.publications') }}">
                            <div class="input-group">
                                <input type="hidden" name="sort" value="{{ request('sort') }}">
                                <input type="text" name="search" class="form-control" placeholder="Cari topik.." style="max-width: 300px" value="{{ request('search') }}">
                                <div class="input-group-append">
                                    <button class="btn btn-primary">Cari</button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="col-6 text-right">
                        <a href="{{ route('admin.publications.topics.add') }}" class="btn btn-primary">Topik Baru</a>
                    </div>
                </div>
                <ul class="nav nav-tabs">
                    <li class="nav-item">
                        <a class="nav-link" aria-current="page" href="{{ route('admin.publications') }}">Publikasi</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link active" href="{{ route('admin.publications.topics') }}">Topik</a>
                    </li>
                </ul>
                <div class="widget widget-content-area br-4">
                    <div class="widget-one">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Nama</th>
                                    <th>Bahasa</th>
                                    <th>Tanggal</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                @if ($topics->count() < 1)
                                    <tr>
                                        <td colspan="5" align="center">Belum ada publikasi</td>
                                    </tr>
                                @endif

                                @foreach ($topics as $topic)
                                    <tr>
                                        <td>{{ $topic->id }}</td>
                                        <td>{{ $topic->name }}</td>
                                        <td>{{ $topic->language() }}</td>
                                        <td>{{ $topic->created_at->format('d F Y, H:i') }}</td>
                                        <td>
                                            <a href="{{ route('admin.publications.topics.edit', $topic->id) }}" class="btn btn-sm btn-primary m-1">Ubah</a>
                                            <button class="btn btn-sm btn-danger m-1" data-toggle="popconfirm" data-url="{{ route('admin.publications.topics.delete', $topic->id) }}">Hapus</button>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                        {{ $topics->links('vendor.pagination.bootstrap-4') }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
