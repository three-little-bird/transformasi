@extends('admin.layouts.dashboard', [
    'menuActive' => 'donasi'
])

@section('content')
    <div class="layout-px-spacing">
        <div class="row">
            <div class="col-xl-12 col-lg-12 col-md-12 col-12 layout-top-spacing layout-spacing">
                <div class="row mb-2">
                    <div class="col-6">
                        <form action="{{ route('admin.donations') }}">
                            <div class="input-group">
                                <input type="hidden" name="sort" value="{{ request('sort') }}">
                                <input type="text" name="search" class="form-control" placeholder="Cari donasi.." style="max-width: 300px" value="{{ request('search') }}">
                                <div class="input-group-append">
                                    <button class="btn btn-primary">Cari</button>
                                </div>
                            </div>
                        </form>
                    </div>
                    {{-- <div class="col-6 text-right">
                        <button class="btn btn-primary">Vidio Baru</button>
                    </div> --}}
                </div>
                <div class="widget widget-content-area br-4">
                    <div class="widget-one">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Nama</th>
                                    <th>Email</th>
                                    <th>Kontak</th>
                                    <th>Jumlah Donasi</th>
                                    <th>Tanggal</th>
                                    {{-- <th></th> --}}
                                </tr>
                            </thead>
                            <tbody>
                                @if ($donors->count() < 1)
                                    <tr>
                                        <td colspan="5" align="center">Belum ada donasi</td>
                                    </tr>
                                @endif

                                @foreach ($donors as $donor)
                                    <tr>
                                        <td>{{ $donor->id }}</td>
                                        <td>{{ $donor->name }}</td>
                                        <td>{{ $donor->email }}</td>
                                        <td>{{ $donor->phone }}</td>
                                        <td>Rp{{ number_format($donor->amount, 0, ',', '.') }}</td>
                                        <td>{{ $donor->created_at->format('d M Y, H:i') }}</td>
                                        {{-- <td>
                                            <a href="{{ route('admin.videos.edit', $donor->id) }}" class="btn btn-sm btn-primary m-1">Ubah</a>
                                            <button class="btn btn-sm btn-danger m-1" data-toggle="popconfirm" data-url="{{ route('admin.videos.delete', $donor->id) }}">Hapus</button>
                                        </td> --}}
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                        {{ $donors->links('vendor.pagination.bootstrap-4') }}
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
