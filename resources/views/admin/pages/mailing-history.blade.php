@extends('admin.layouts.dashboard', [
    'menuActive' => 'mailing'
])

@section('content')
    <div class="layout-px-spacing">
        <div class="row">
            <div class="col-xl-12 col-lg-12 col-md-12 col-12 layout-top-spacing layout-spacing">
                <div class="row mb-2">
                    <div class="col-6">
                        <form action="{{ route('admin.mailing.history') }}">
                            <div class="input-group">
                                <input type="hidden" name="sort" value="{{ request('sort') }}">
                                <input type="text" name="search" class="form-control" placeholder="Cari mailing.." style="max-width: 300px" value="{{ request('search') }}">
                                <div class="input-group-append">
                                    <button class="btn btn-primary">Cari</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="widget widget-content-area br-4">
                    <div class="widget-one">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Subject</th>
                                    <th>Message</th>
                                    <th>Status</th>
                                    <th>Tanggal</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                @if ($blasts->count() < 1)
                                    <tr>
                                        <td colspan="5" align="center">Belum ada blast</td>
                                    </tr>
                                @endif

                                @foreach ($blasts as $blast)
                                    <tr>
                                        <td>{{ $blast->id }}</td>
                                        <td>{{ $blast->subject }}</td>
                                        <td>{{ \Illuminate\Support\Str::limit(strip_tags($blast->content), 255) }}</td>
                                        <td>
                                            @if($blast->status == 'Queue')
                                                <span class="badge badge-warning">Dalam antrian</span>
                                            @elseif($blast->status == 'Sent')
                                                <span class="badge badge-success">Selesai</span>
                                            @else
                                                <span class="badge badge-danger">{{ $blast->status }}</span>
                                            @endif
                                        </td>
                                        <td>{{ $blast->created_at->format('d M Y, H:i') }}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                        {{ $blasts->links('vendor.pagination.bootstrap-4') }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
