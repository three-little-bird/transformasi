<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!-->
<html class="no-js" lang="en">
    <!--<![endif]-->

    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="DynamicLayers">

        <title>
            @if (isset($title))
                {{ $title }}
            @else
                {{ __('main.title') }}
            @endif
        </title>

        <link rel="shortcut icon" type="image/x-icon" href="{{ asset('favicon.ico') }}">

        <!-- Plugins CSS -->
        <link rel="stylesheet" href="{{ asset('css/plugins.css') }}?v={{ filemtime('css/plugins.css') }}">
        <!-- Main CSS -->
        <link rel="stylesheet" href="{{ asset('css/app.css') }}?v={{ filemtime('css/app.css') }}">
        {{-- <link rel="stylesheet" href="{{ asset('assets/css/main.css') }}?v={{ filemtime('assets/css/main.css') }}">
        <link rel="stylesheet" href="{{ asset('assets/css/responsive.css') }}?v={{ filemtime('assets/css/responsive.css') }}"> --}}
        <!-- Google Fonts -->
        <link rel="preconnect" href="https://fonts.gstatic.com">
        <link href="https://fonts.googleapis.com/css2?family=Oswald:wght@300&display=swap" rel="stylesheet">

        <script src="{{ asset('assets/js/vendor/modernizr-2.8.3-respond-1.4.2.min.js') }}"></script>
    </head>

    <body>
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

        <div class="site-preloader-wrap">
            <div class="spinner"></div>
        </div><!-- Preloader -->

        @include('web.components.header')

        <div class="header-height"></div>

        <div class="pager-header">
            <div class="container">
                <div class="page-content">
                    <h2>@yield('title')</h2>
                </div>
            </div>
        </div>

        <section class="blog-section bg-grey padding">
            <div class="container">
                @yield('content')
            </div>
        </section>

        @include('web.components.footer')

        <a data-scroll href="#header" id="scroll-to-top"><i class="fa fa-arrow-circle-up fa-5x"></i></a>

        <!-- jQuery Lib -->
        <script src="{{ asset('assets/js/vendor/jquery-1.12.4.min.js') }}"></script>
        <!-- Bootstrap JS -->
        <script src="{{ asset('assets/js/vendor/bootstrap.min.js') }}"></script>
        <!-- Tether JS -->
        <script src="{{ asset('assets/js/vendor/tether.min.js') }}"></script>
        <!-- Imagesloaded JS -->
        <script src="{{ asset('assets/js/vendor/imagesloaded.pkgd.min.js') }}"></script>
        <!-- OWL-Carousel JS -->
        <script src="{{ asset('assets/js/vendor/owl.carousel.min.js') }}"></script>
        <!-- isotope JS -->
        <script src="{{ asset('assets/js/vendor/jquery.isotope.v3.0.2.js') }}"></script>
        <!-- Smooth Scroll JS -->
        <script src="{{ asset('assets/js/vendor/smooth-scroll.min.js') }}"></script>
        <!-- venobox JS -->
        <script src="{{ asset('assets/js/vendor/venobox.min.js') }}"></script>
        <!-- ajaxchimp JS -->
        <script src="{{ asset('assets/js/vendor/jquery.ajaxchimp.min.js') }}"></script>
        <!-- Counterup JS -->
        <script src="{{ asset('assets/js/vendor/jquery.counterup.min.js') }}"></script>
        <!-- waypoints js -->
        <script src="{{ asset('assets/js/vendor/jquery.waypoints.v2.0.3.min.js') }}"></script>
        <!-- Slick Nav JS -->
        <script src="{{ asset('assets/js/vendor/jquery.slicknav.min.js') }}"></script>
        <!-- Nivo Slider JS -->
        <script src="{{ asset('assets/js/vendor/jquery.nivo.slider.pack.js') }}"></script>
        <!-- Letter Animation JS -->
        <script src="{{ asset('assets/js/vendor/letteranimation.min.js') }}"></script>
        <!-- Wow JS -->
        <script src="{{ asset('assets/js/vendor/wow.min.js') }}"></script>
        <!-- Main JS -->
        {{-- <script src="{{ asset('assets/js/contact.js') }}"></script> --}}
        {{-- <script src="{{ asset('assets/js/main.js?v=1') }}"></script> --}}
        <script src="{{ asset('js/app.js') }}?v={{ filemtime('js/app.js') }}"></script>

    </body>

</html>