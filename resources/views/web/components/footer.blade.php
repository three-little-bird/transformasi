<section class="widget-section padding-footer">
    <div class="container-fluid">
        <div class="widget-wrap row">
            <!-- <div class="widget-content col-md-12 row ">
                <div class=" col-md-12 row m-auto">
                    <div class="col-1">
                        <img src="{{ asset('assets/img/logo-white.png') }}" alt="logo">
                    </div>
                </div>
                <div class=" col-md-12 row m-auto">
                    <div class="col-4">
                        <a href="https://facebook.com/pusattransformasikebijakanpublik/?_rdc=1&_rdr" class="socmed-btn bg-light" >
                            <i class="fa fa-facebook fa-2x m-auto"> 
                            </i>
                        </a>                           
                    </div>
                    <div class="col-4">
                        <a href="https://twitter.com/transformasi_id" class="socmed-btn bg-light" >
                            <i class="fa fa-twitter fa-2x"> 
                            </i>
                        </a>                           
                    </div>
                    <div class="col-4">
                        <a href="https://www.youtube.com/channel/UC4ZVgg9Hh8vo_CIz9OKj7KQ/videos" class="socmed-btn bg-light" >
                            <i class="fa fa-youtube-play fa-2x m-auto"> 
                            </i>
                        </a>                           
                    </div>
                </div>
                <div class="spacer-20"></div>
                <p class="footer-text" >
                    &copy; 2021 All Rights Reserved | Transformasi Kebijakan Publik
                </p>
            </div>   -->
            @if(getLocale() == 'id-ID')
            <div class="col-md-4 xs-padding widget-content row">
                <div class="col-12">
                   <img src="{{ asset('assets/img/logo-white.png') }}" alt="logo" class="w-100">
                </div>
                <div class="col-4">
                    <a href="https://facebook.com/pusattransformasikebijakanpublik/?_rdc=1&_rdr" class="socmed-btn bg-light" >
                        <i class="fa fa-facebook fa-2x m-auto"> 
                        </i>
                    </a>                           
                </div>
                <div class="col-4">
                    <a href="https://twitter.com/transformasi_id" class="socmed-btn bg-light" >
                        <i class="fa fa-twitter fa-2x"> 
                        </i>
                    </a>                           
                </div>
                <div class="col-4">
                    <a href="https://www.youtube.com/channel/UC4ZVgg9Hh8vo_CIz9OKj7KQ/videos" class="socmed-btn bg-light" >
                        <i class="fa fa-youtube-play fa-2x m-auto"> 
                        </i>
                    </a>                               
                </div>
            </div>
            <div class="col-md-4">
                <div class="widget-content ">
                    <h3 class="text-center">Tentang Transformasi</h3>
                    <p class="footer-text">
                    Pusat Transformasi Kebijakan Publik (Transformasi) adalah jaringan para pemikir yang melibatkan para pengambil keputusan, akademisi dan publik untuk berkontribusi pada pembuatan kebijakan publik yang  lebih baik di Indonesia dengan berbasis data dan fakta. Transformasi telah mengimplementasikan 
                    program di bidang Adaptasi Perubahan Iklim; Kelautan dan Perikanan; Ketenagakerjaan Inklusif; serta 
                    Pengelolaan Sampah. 
                    </p>
                </div> 
            </div>
            <div class="col-md-4 xs-padding">
                <div class="widget-content">
                    <div class="row justify-content-center">
                        <div class="col-12">
                            <div class="bg-primary  text-white">
                                <h3 class="bd-bottom text-center mb-3">
                                    Dapatkan Berita Terbaru 
                                </h3>
                                <div class="row d-flex my-2 pr-2 pr-md-5 div1 mb-3 ">
                                    <div class="col-md-12">
                                        <form action="{{ route('home.newsletter-subscribe') }}" method="POST">
                                            @csrf
                                            <input type="email" name="email" class="form-control py-3 newsletter-email" id="inp1" placeholder="Masukkan email anda..."> 
                                            <button class="default-btn bg-tersier btn-newsletter">
                                                <h4 class="text-light">Subscribe</h4>
                                            </button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <ul class="address bd-top">
                        <li class="footer-text mt-3">
                            <i class="fa fa-envelope"></i> 
                            info@transformasi.org                       
                        </li>
                        <li class="footer-text">
                            <i class="fa fa-phone"></i> 0812-957-1105
                        </li>
                        <li class="footer-text"><i class="fa fa-globe"></i> www.transformasi.org</li>
                    </ul>
                </div>
            </div>
            @else
            <div class="col-md-4 xs-padding widget-content row">
                <div class="col-12">
                   <img src="{{ asset('assets/img/logo-white.png') }}" alt="logo" class="w-100">
                </div>
                <div class="col-4">
                    <a href="https://facebook.com/pusattransformasikebijakanpublik/?_rdc=1&_rdr" class="socmed-btn bg-light" >
                        <i class="fa fa-facebook fa-2x m-auto"> 
                        </i>
                    </a>                           
                </div>
                <div class="col-4">
                    <a href="https://twitter.com/transformasi_id" class="socmed-btn bg-light" >
                        <i class="fa fa-twitter fa-2x"> 
                        </i>
                    </a>                           
                </div>
                <div class="col-4">
                    <a href="https://www.youtube.com/channel/UC4ZVgg9Hh8vo_CIz9OKj7KQ/videos" class="socmed-btn bg-light" >
                        <i class="fa fa-youtube-play fa-2x m-auto"> 
                        </i>
                    </a>                               
                </div>
            </div>
            <div class="col-md-4">
                <div class="widget-content ">
                    <h3 class="text-center">About Transformasi</h3>
                    <p class="footer-text">
                        The Center for Public Policy Transformation (Transformasi) is a networked think tank of decision makers, scholars, and the public to contribute to a better evidence-based public policy making in Indonesia.  Transformasi has implemented a number of programs in Climate Change Adaptation; Marine and Fisheries, Inclusive Workforce; as well as Waste Management.
                    </p>
                </div> 
            </div>
            <div class="col-md-4 xs-padding">
                <div class="widget-content">
                    <div class="row justify-content-center">
                        <div class="col-12">
                            <div class="bg-primary  text-white">
                                <h3 class="bd-bottom text-center mb-3">
                                    Subscribe To Our Newsletter 
                                </h3>
                                <div class="row d-flex my-2 pr-2 pr-md-5 div1 mb-3 ">
                                    <div class="col-md-12">
                                        <form action="{{ route('home.newsletter-subscribe') }}" method="POST">
                                            @csrf
                                            <input type="email" name="email" class="form-control py-3 newsletter-email w-100" id="inp1" placeholder="Enter your email address..."> 
                                            <button class="default-btn bg-tersier btn-newsletter">
                                                <span class="text-light">Subscribe</span>
                                            </button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <ul class="address bd-top">
                        <li class="footer-text mt-3">
                            <i class="fa fa-envelope"></i> 
                            info@transformasi.org                       
                        </li>
                        <li class="footer-text">
                            <i class="fa fa-phone"></i> 0812-957-1105
                        </li>
                        <li class="footer-text"><i class="fa fa-globe"></i> www.transformasi.org</li>
                    </ul>
                </div>
            </div>
            @endif
        </div>
    </div>
</section><!-- ./Widget Section -->
