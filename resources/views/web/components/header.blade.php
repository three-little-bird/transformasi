<header id="header" class="header-section">
    <div class="top-header d-xl-none"> 
        <div class="container">
            <div class="sl-nav mt-auto w-100 d-block">
                <ul class="">
                <li>
                    @if(getLocale() == 'id-ID')
                        <b class="text-white">ID <i class="fa fa-angle-down" aria-hidden="true"></i></b>
                        <ul class="text-dark">
                            <li>
                                <a href="#" class="d-block">
                                    <img src="/assets/img/lang/id.gif"> <span class="lang-active">ID</span>
                                </a>
                            </li>
                            <li>
                                <a href="/?hl=en" class="d-block">
                                    <img src="/assets/img/lang/en.gif"> <span class="">EN</span>
                                </a>
                            </li>
                        </ul>
                    @else
                        <b class="text-white">EN <i class="fa fa-angle-down" aria-hidden="true"></i></b>
                        <ul class="text-dark">
                            <li>
                                <a href="#" class="d-block">
                                    <img src="/assets/img/lang/en.gif"> <span class="lang-active">EN</span>
                                </a>
                            </li>
                            <li>
                                <a href="/?hl=id" class="d-block">
                                    <img src="/assets/img/lang/id.gif"> <span class="">ID</span>
                                </a>
                            </li>
                        </ul>
                    @endif
                </li>
                </ul>
            </div>               
        </div>
    </div>
    <div class="bottom-header">
        <div class="container">
            <div class="bottom-content-wrap row">
                <div class="col-lg-3">
                    <div class="site-branding">
                        <a href="{{ route('home') }}"><img src="{{ asset('assets/img/logo.png') }}" alt="Brand" class="my-3 w-75"></a>
                    </div>
                </div>
                <div class="col-lg-9 text-right">
                    @if(getLocale() == 'id-ID')
                        <ul id="mainmenu" class="nav navbar-nav nav-menu text-light">
                            <li class="active"> <a href="{{ url('/') }}">Beranda</a>

                            </li>
                            <li><a href="javascript:;">Tentang Kami</a>
                                <ul>
                                    <li><a href="{{ url('/profil') }}">Profil</a></li>
                                    <li><a href="{{ url('/dewan-penasihat') }}">Dewan Penasihat</a></li>
                                    <li><a href="{{ url('/tim-manajemen') }}">Tim Manejemen</a></li>
                                    <li><a href="{{ url('/donor-dan-mitra') }}">Donor & Mitra</a></li>
                                    <li><a href="{{ url('/hubungi-kami') }}">Hubungi Kami</a></li>
                                </ul>
                            </li>
                            <li><a href="javascript:;">Wilayah Kerja</a>
                                <ul>
                                    <li><a href="{{ url('/wilayah-kerja-aceh') }}">Aceh</a></li>
                                    <li><a href="{{ url('/wilayah-kerja-jabar') }}">Jawa Barat</a></li>
                                    <li><a href="{{ url('/wilayah-kerja-jateng') }}">Jawa Tengah</a></li>
                                    <li><a href="{{ url('/wilayah-kerja-jatim') }}">Jawa Timur</a></li>
                                    <li><a href="{{ url('/wilayah-kerja-gorontalo') }}">Gorontalo</a></li>
                                    <li><a href="{{ url('/wilayah-kerja-sulsel') }}">Sulawesi Selatan</a></li>
                                    <li><a href="{{ url('/wilayah-kerja-ntb') }}">Nusa Tenggara Barat</a></li>
                                    <li><a href="{{ url('/wilayah-kerja-ntt') }}">Nusa Tenggara Timur</a></li>
                                    <li><a href="{{ url('/wilayah-kerja-malut') }}">Maluku Utara</a></li>
                                </ul>
                            </li>
                            <li><a href="javascript:;">Program</a>
                                <ul>
                                    <li><a href="{{ url('/ketenagakerjaan-inklusif') }}">Ketenagakerjaan Inklusif</a></li>
                                    <li><a href="{{ url('/pengelolaan-sampah') }}">Pengelolaan Sampah</a></li>
                                    <li><a href="{{ url('/perikanan-dan-kelautan') }}">Perikanan &amp; Kelautan</a></li>
                                    <li><a href="{{ url('/lingkungan-hidup-dan-kehutanan') }}">Lingkungan Hidup &amp; Kehutanan</a></li>
                                    <li><a href="{{ url('/riset-kebijakan') }}">Riset Kebijakan</a></li>
                                </ul>
                            </li>
                            <li><a href="{{ url('/ikut-terlibat') }}">Ikut Terlibat</a>
                            </li>
                            <li><a href="javascript:;">Publikasi</a>
                                <ul>
                                    <li><a href="{{ url('/publications?type=policy-brief') }}">Ringkasan Kebijakan</a></li>
                                    <li><a href="{{ url('/publications?type=book') }}">Buku</a></li>
                                    <li><a href="{{ url('/publications?type=data') }}">Data</a></li>
                                    <li><a href="{{ url('/publications?type=infographic') }}">Infografi</a></li>
                                    <li><a href="{{ url('/articles') }}">Artikel</a></li>
                                    <li><a href="{{ url('/articles/category/145-umum') }}">Berita</a></li>
                                </ul>
                            </li>
                            <li><a href="javascript:;">Galeri</a>
                                <ul>
                                    <li><a href="{{ url('/galleries') }}">Foto</a></li>
                                    <li><a href="{{ url('/videos') }}">Video</a></li>
                                </ul>
                            </li>
                            <li> 
                                <a href="{{ url('/donate') }}">
                                    <span class="btn default-btn bg-tersier d-block w-100 w-md-auto">Donasi</span>
                                </a>
                            </li>
                        </ul>
                        <div class="sl-nav mt-auto d-none d-xl-inline">
                            <ul>
                              <li><b class="text-white">ID</b> <i class="fa fa-angle-down" aria-hidden="true"></i>
                                <ul class="text-dark">
                                    <li>
                                        <a href="#" class="d-block">
                                            <img src="/assets/img/lang/id.gif"> <span class="lang-active">ID</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="/?hl=en" class="d-block">
                                            <img src="/assets/img/lang/en.gif"> <span class="">EN</span>
                                        </a>
                                    </li>
                                </ul>
                              </li>
                            </ul>
                        </div>
                    @else
                        <ul id="mainmenu" class="nav navbar-nav nav-menu text-light">
                            <li class="active"> <a href="{{ url('/') }}">Home</a>
                            </li>
                            <li><a href="javascript:;">About Us</a>
                                <ul>
                                    <li><a href="{{ url('/profile') }}">Profile</a></li>
                                    <li><a href="{{ url('/advisory-board') }}">Our Advisory Board</a></li>
                                    <li><a href="{{ url('/management-team') }}">Management Team</a></li>
                                    <li><a href="{{ url('/donors-partners') }}">Our Donors and Partners</a></li>
                                    <li><a href="{{ url('/contact-us') }}">Contact Us</a></li>

                                </ul>
                            </li>
                            <li><a href="javascript:;">Locations</a>
                                <ul>
                                    <li><a href="{{ url('/location-aceh') }}">Aceh</a></li>
                                    <li><a href="{{ url('/location-west-java') }}">West Java</a></li>
                                    <li><a href="{{ url('/location-central-java') }}">Central Java</a></li>
                                    <li><a href="{{ url('/location-east-java') }}">East Java</a></li>
                                    <li><a href="{{ url('/location-gorontalo') }}">Gorontalo</a></li>
                                    <li><a href="{{ url('/location-south-sulawesi') }}">South Sulawesi</a></li>
                                    <li><a href="{{ url('/location-southeast-west-nusa') }}">West Nusa Tenggara</a></li>
                                    <li><a href="{{ url('/location-southeast-east-nusa') }}">East Nusa Tenggara</a></li>
                                    <li><a href="{{ url('/location-north-maluku') }}">North Maluku</a></li>
                                </ul>
                            </li>
                            <li><a href="javascript:;">Program</a>
                                <ul>
                                    <li><a href="{{ url('/inclusive-workforce') }}">Inclusive Workforce Development</a></li>
                                    <li><a href="{{ url('/waste-management') }}">Waste Management</a></li>
                                    <li><a href="{{ url('/fisheries-and-marine') }}">Fisheries &amp; Marine</a></li>
                                    <li><a href="{{ url('/environtment-and-forestry') }}">Environtment &amp; Forestry</a></li>
                                    <li><a href="{{ url('/policy-research') }}">Policy Research</a></li>
                                </ul>
                            </li>
                            <li>
                                <a href="{{ url('/join-us') }}">Get Involved</a>
                            </li>
                            <li><a href="javascript:;">Publication</a>
                                <ul>
                                    <li><a href="{{ url('/publications?type=policy-brief') }}">Policy Brief</a></li>
                                    <li><a href="{{ url('/publications?type=book') }}">Book</a></li>
                                    <li><a href="{{ url('/publications?type=data') }}">Data</a></li>
                                    <li><a href="{{ url('/publications?type=infographic') }}">Infographics</a></li>
                                    <li><a href="{{ url('/articles') }}">Article</a></li>
                                    <li><a href="{{ url('/articles/category/145-umum') }}">News</a></li>
                                </ul>
                            </li>
                            <li><a href="javascript:;">Gallery</a>
                                <ul>
                                    <li><a href="{{ url('/galleries') }}">Photos</a></li>
                                    <li><a href="{{ url('/videos') }}">Video</a></li>
                                </ul>
                            </li>
                            <li> <a href="{{ url('/donate') }}l"><span class="btn default-btn bg-tersier">Donation</span></a></li>    
                        </ul>
                        <div class="sl-nav mt-auto d-none d-xl-inline">
                            <ul>
                              <li><b class="text-white">EN</b> <i class="fa fa-angle-down" aria-hidden="true"></i>
                                <ul class="text-dark">
                                    <li>
                                        <a href="/?hl=id" class="d-block">
                                            <img src="/assets/img/lang/id.gif"> <span class="">ID</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#" class="d-block">
                                            <img src="/assets/img/lang/en.gif"> <span class="lang-active">EN</span>
                                        </a>
                                    </li>
                                </ul>
                              </li>
                            </ul>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</header><!-- /Header Section -->