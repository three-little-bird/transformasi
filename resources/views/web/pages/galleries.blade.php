@extends('web.layouts.default', [
    'title' => "Galeri Foto - Transformasi"
])

@section('content')
    <div class="pager-header">
        <div class="container">
            <div class="page-content">
                <h2>Galeri Foto</h2>
            </div>
        </div>
    </div><!-- /Page Header -->

    <section class="gallery-section bd-bottom padding">
        <div class="container">
            <div class="gallery-item  row">
                <div class="col-lg-3 sm-padding right-sidebar">
                    <div class="sidebar-wrap sticky">
                        <div class="sidebar-widget mb-50">
                            <h4>Daftar Album</h4>
                            <ul class="cat-list accordion">
                                @php
                                    $year = null;
                                @endphp
                                @foreach ($galleries as $album)
                                    @if ($album->created_at->format('Y') != $year && $year !== null)
                                            </ul>
                                        </li>
                                    @endif
                                    @if ($album->created_at->format('Y') != $year)
                                        @php
                                            $year = $album->created_at->format('Y');
                                        @endphp
                                        <li class="mt-2" data-toggle="collapse" data-target="#collapse{{ $album->created_at->format('Y') }}" style="cursor: pointer;"><b>{{ $album->created_at->format('Y') }}</b>
                                            <ul id="collapse{{ $album->created_at->format('Y') }}" class="collapse {{ $gallery->created_at->format('Y') == $year ? 'show' : '' }}">
                                    @endif

                                    <li class="ml-2 mb-2 border-top">
                                        <a href="{{ route('home.galleries', $album->id) }}" class="{{ $album->id == $gallery->id ? 'text-primary font-weight-bold' : '' }}" style="line-height: 24px">{{ $album->title }}</a>
                                        <span>({{ $album->photos_count }})</span>
                                    </li>
                                @endforeach
                            </ul>
                        </div><!-- Categories -->
                    </div><!-- /Sidebar Wrapper -->
                </div>
                <div class="col-lg-9 justify-content-center">
                    <div class="row gallery-wraps">
                        <div class="col-lg-12 text-center">
                            <h2>{{ $gallery->title }}</h2>
                        </div>
                        @foreach ($gallery->photos as $photo)
                            <div class="col-lg-4 col-sm-6 wordpress design branding ">
                                <div class="gallery-wrap">
                                    <img src="{{ url($photo->image_path) }}" class="rounded" alt="gallery">
                                    <div class="hover">
                                        <a class="img-popup" data-gall="galleryimg" href="{{ url($photo->image_path) }}"><i class="ti-image"></i></a>
                                        <a class="ml-2" href="{{ route('home.photo.download', $photo->id) }}"><i class="ti-download"></i></a>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </section><!-- /Gallery Section -->
@endsection