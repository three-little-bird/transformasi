@extends('web.layouts.default', [
    'title' => "Publikasi - Transformasi"
])

@section('content')
    <div class="pager-header">
        <div class="container">
            <div class="page-content">
                <h2>Publikasi</h2>
            </div>
        </div>
    </div><!-- /Page Header -->

    <section class="blog-section bg-grey padding">
        <div class="container">
            <div class="row">
                <div class="col-lg-9 xs-padding">
                    <div class="blog-items right-sidebar row">
                        @foreach ($publications as $publication)
                            <div class="col-md-12 padding-15 row publikasi">
                                <div class="col-md-6 pr-0">
                                    <div class="blog-post p0">
                                        <img src="{{ $publication->image_url ? asset($publication->image_url) : config('app.default_image') }}" alt="blog post">
                                    </div>
                                </div>
                                <div class="col-md-6 pl-0">
                                    <div class="blog-content">
                                        <span class="date"><i class="fa fa-clock-o"></i> {{ $publication->created_at->format('d F Y') }}</span>
                                        <h3><a href="{{ route('home.publication.view', $publication->id) }}">{{ $publication->title }}</a></h3>
                                        <ul>
                                            @if ($publication->topic)
                                                <li>
                                                    Topik : {{ $publication->topic->name }}
                                                </li>
                                            @endif
                                            <li>
                                                Tipe : {{ $typeText[$publication->type] }}
                                            </li>
                                            @if ($publication->author)
                                                <li>
                                                    Author : {{ $publication->author }}
                                                </li>
                                            @endif
                                            <li>
                                                <hr class="my-2">
                                                <div>{!! \Illuminate\Support\Str::limit(strip_tags(htmlspecialchars_decode($publication->description))) !!}</div>
                                            </li>
                                            @if ($publication->getAttachmentIds())
                                                <li>
                                                    <hr class="my-2">
                                                    @foreach ($publication->getAttachmentIds() as $attachment_id)
                                                        @if (!isset($attachments[$attachment_id]))
                                                            @continue
                                                        @endif
                                                        <div class="alert alert-info mb-2">
                                                            <a href="{{ route('home.attachment.download', $attachment_id) }}"><small><b><i class="fa fa-download"></i> &nbsp; {{ $attachments[$attachment_id]['title'] }} ({{ $attachments[$attachment_id]['download_count'] }} downloads)</b></small></a>
                                                        </div>
                                                    @endforeach
                                                </li>
                                            @endif
                                        </ul>
                                        <a href="{{ route('home.publication.view', $publication->id) }}" class="post-meta btn btn-sm btn-primary">Selengkapnya <i class="fa fa-arrow-circle-right"></i></a>
                                    </div>
                                </div>
                            </div>
                        <div class="spacer-single"></div>
                        @endforeach
                    </div>
                    {{ $publications->links('vendor.pagination.custom') }}
                </div><!-- Blog Posts -->
                <div class="col-lg-3 xs-padding">
                    <div class="sidebar-wrap sticky">
                        <form action="{{ url()->current() }}" class="search-form">
                            <div class="sidebar-widget">
                                <h4>Cari Publikasi</h4>
                                <input type="text" name="q" class="form-control" placeholder="Masukkan Kata Kunci..." value="{{ request('q') }}">
                            </div>
                            <div class="spacer-single"></div>
                            <div class="sidebar-widget">
                                <select class="custom-select" name="topic_id">
                                    <option value="" selected>-- Topik --</option>
                                    @foreach ($topics as $topic)
                                        <option value="{{ $topic->id }}" @if(request('topic_id') == $topic->id) selected @endif>{{ $topic->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="spacer-half"></div>
                            <div class="sidebar-widget">
                                <select class="custom-select" name="type">
                                    <option value="" selected>-- Tipe --</option>
                                    @foreach ($typeText as $type => $text)
                                        <option value="{{ $type }}" @if(request('type') == $type) selected @endif>{{ $text }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="spacer-half"></div>
                            @if ($authors && !($authors[0] == null && count($authors) == 1))
                                <div class="sidebar-widget">
                                    <select class="custom-select" name="author">
                                        <option value="" selected>-- Penulis --</option>
                                        @foreach ($authors as $author)
                                            @if (!$author)
                                                @continue
                                            @endif
                                            <option value="{{ $author }}" @if(request('author') == $author) selected @endif>{{ $author }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="spacer-half"></div>
                            @endif
                            <div class="sidebar-widget">
                                <select class="custom-select" name="year">
                                    <option value="" selected>-- Tahun --</option>
                                    @for ($i = date('Y'); $i >= 2000; $i--)
                                        <option value="{{ $i }}" @if(request('year') == $i) selected @endif>{{ $i }}</option>
                                    @endfor
                                </select>
                            </div>
                            <div class="spacer-single"></div>
                            <div class="sidebar-widget">
                                <button id="submit" class="btn btn-primary btn-block" type="submit">Cari</button>
                            </div>
                        </form>

                    </div><!-- /Sidebar Wrapper -->
                </div>
            </div>
        </div>
    </section><!-- /Blog Section -->
@endsection