@extends('web.layouts.default', [
    'title' => "Vidio - Transformasi"
])

@section('content')
    <div class="pager-header">
        <div class="container">
            <div class="page-content">
                <h2>Galeri Video</h2>
            </div>
        </div>
    </div><!-- /Page Header -->

    <section class="gallery-section bd-bottom padding bg-grey">
        <div class="container-fluid">
            <div class="gallery-item  row">
                <div class="col-lg-3 sm-padding">
                    <div class="sidebar-wrap  sticky right-sidebar ">
                        <div class="sidebar-widget mb-50">
                            <h4>Daftar Album</h4>
                            <ul class="cat-list">
                                @foreach ($years as $year)
                                    <li class="mt-2"><a href="{{ route('home.videos', $year) }}" class="{{ $year == request('year') ? 'text-primary' : '' }}">{{ $year }}</a></li>
                                @endforeach
                            </ul>
                        </div><!-- Categories -->
                    </div><!-- /Sidebar Wrapper -->
                </div>
                <div class="col-lg-9">
                    <div class="row">
                        @foreach ($videos as $video)
                            <div class=" col-md-12 row card-video bg-white p-3 ml-auto" style="width: 18rem;">
                                <div class="col-md-5">
                                    <div class="">
                                        <h2 class=""><b>{{ $video->title }}</b></h2>
                                        <p class="">{!! $video->description !!}</p>
                                    </div>
                                    <a href="https://www.youtube.com/channel/UC4ZVgg9Hh8vo_CIz9OKj7KQ" class="default-btn bg-tersier">Lihat Channel Youtube Kami</a>
                                </div>
                                <div class="video-container col-md-7">
                                    {!! $video->embedcode !!}
                                </div>
                            </div>
                            <div class="spacer-20 bg-grey">
                            </div>
                        @endforeach
                    </div>
                    {{ $videos->links('vendor.pagination.custom') }}
                </div>
            </div>
        </div>
    </section><!-- /Gallery Section -->
@endsection