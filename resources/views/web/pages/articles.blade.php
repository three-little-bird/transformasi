@extends('web.layouts.default', [
    'title' => "Berita & Kegiatan Terkini - Transformasi"
])

@section('content')
    {{-- <section class="events-section bg-grey bd-bottom padding">
        <div class="container">
            <div class="section-heading text-center mb-40">
                <h2>Berita &amp; Kegiatan Terkini</h2>
                <span class="heading-border"></span>
                <p>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Repellat, aperiam!</p>
            </div><!-- /Section Heading -->
            <div id="event-carousel" class="events-wrap owl-Carousel">
                <div class="events-item">
                    <div class="event-thumb">
                        <img src="/assets/img/events-1.jpg" alt="events">
                    </div>
                    <div class="event-details">
                        <h3>Dummy Event 1</h3>
                        <div class="event-info">
                            <p><i class="ti-calendar"></i>Started On: 7:00 AM.</p>
                            <p><i class="ti-location-pin"></i>Zoom</p>
                        </div>
                        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Nostrum at consequuntur
                            obcaecati dolor perferendis?</p>
                        <a href="#" class="default-btn">Read More</a>
                    </div>
                </div><!-- Event-1 -->
                <div class="events-item">
                    <div class="event-thumb">
                        <img src="/assets/img/events-2.jpg" alt="events">
                    </div>
                    <div class="event-details">
                        <h3>Dummy Event 2</h3>
                        <div class="event-info">
                            <p><i class="ti-calendar"></i>Started On: 3:00 PM.</p>
                            <p><i class="ti-location-pin"></i>Zoom</p>
                        </div>
                        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Nostrum at consequuntur
                            obcaecati dolor perferendis?</p>
                        <a href="#" class="default-btn">Read More</a>
                    </div>
                </div><!-- Event-2 -->
                <div class="events-item">
                    <div class="event-thumb">
                        <img src="/assets/img/events-3.jpg" alt="events">
                    </div>
                    <div class="event-details">
                        <h3>Dummy Event 3</h3>
                        <div class="event-info">
                            <p><i class="ti-calendar"></i>Started On: 7:00 PM.</p>
                            <p><i class="ti-location-pin"></i>Zoom</p>
                        </div>
                        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Nostrum at consequuntur
                            obcaecati temporibus dolor perferendis?</p>
                        <a href="#" class="default-btn">Read More</a>
                    </div>
                </div><!-- Event-3 -->
        <div class="events-item">
                    <div class="event-thumb">
                        <img src="https://www.transformasi.org/thetumb.php?w=400&amp;h=200&amp;src=images/artikel/47865870-b529-4505-9b96-df9ffe816bd3.jpg" alt="events">
                    </div>
                    <div class="event-details">
                        <h3>Dummy Event 3</h3>
                        <div class="event-info">
                            <p><i class="ti-calendar"></i>Started On: 7:00 PM.</p>
                            <p><i class="ti-location-pin"></i>Zoom</p>
                        </div>
                        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Nostrum at consequuntur
                            obcaecati temporibus dolor perferendis?</p>
                        <a href="#" class="default-btn">Read More</a>
                    </div>
                </div><!-- Event-3 -->
            </div>

        </div>
    </section> --}}

    <section class="blog-section bg-grey padding">
        <div class="container">
        <div class="section-heading text-center mb-40">
               <h2>Berita &amp; Kegiatan Terkini</h2>
                <span class="heading-border"></span>
            </div>
            <div class="spacer-half"></div>
            <div class="row">
                <div class="col-lg-9 xs-padding">
                    <div class="blog-items right-sidebar row">
                        @foreach ($articles as $article)
                            <div class="col-md-6 padding-15">
                                <div class="blog-post">
                                    <img src="{{ $article->thumbnail_url ? url($article->thumbnail_url) : config('app.default_image') }}" alt="blog post">
                                    <div class="blog-content h-350">
                                        <div class="h-90">

                                            <span class="date"><i class="fa fa-clock-o"></i> {{ $article->created_at->format('d F Y') }}</span>
                                            <h3><a href="{{ route('home.article', $article->slug) }}">{{ $article->title }}</a></h3>
                                            <p>{{ \Illuminate\Support\Str::limit(strip_tags(htmlspecialchars_decode($article->content))) }}</p>
                                        </div>
                                        <a href="{{ route('home.article', $article->slug) }}" class="post-meta">{{ __('main.read-more') }} <i class="fa fa-arrow-right"></i></a>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                    {{ $articles->links('vendor.pagination.custom') }}
                </div><!-- Blog Posts -->
                <div class="col-lg-3 xs-padding">
                    <div class="sidebar-wrap sticky">
                        <div class="sidebar-widget mb-50">
                            <h4>Cari Artikel</h4>
                            
                            <form class="search-form">
                                <input type="text" name="q" value="{{ request('q') }}" class="form-control" placeholder="Type here">
                                <button class="search-btn"><i class="fa fa-search"></i></button>
                            </form>
                            
                        </div>
                        <div class="sidebar-widget mb-50">
                            <h4>Kategori</h4>
                            <ul class="cat-list">
                                @foreach ($categories as $category)
                                    <li><a href="{{ route('home.articles.category', $category->id . "-" . $category->slug) }}">{{ $category->name }}</a><span>({{ $category->articles_count }})</span></li>
                                @endforeach
                            </ul>
                        </div><!-- Categories -->
                    </div><!-- /Sidebar Wrapper -->
                </div>
            </div>
        </div>
    </section><!-- /Blog Section -->
@endsection