@extends('web.layouts.default', [
    'title' => $article->title." - Transformasi"
])

@section('meta')
    @if ($article->meta())
        @if (isset($article->meta()['author']) && $article->meta()['author'])
            <meta name="author" content="{{ $article->meta()['author'] ? $article->meta()['author'] : $article->author->name }}" />
        @endif
        @if (isset($article->meta()['description']) && $article->meta()['description'])
            <meta name="description" content="{{ $article->meta()['description'] }}" />
        @endif
        @if (isset($article->meta()['key']) && $article->meta()['key'])
            <meta name="keywords" content="{{ $article->meta()['key'] }}" />
        @endif
        @if (isset($article->meta()['robots']) && $article->meta()['robots'])
            <meta name="robots" content="{{ $article->meta()['robots'] }}" />
        @endif
    @endif
@endsection

@section('content')
    <section class="blog-section bg-grey padding">
        <div class="container">
            <div class="row">
                <div class="col-lg-9 sm-padding">
                    <div class="blog-items right-sidebar single-post">
                        {{-- @if ($article->image_intro_path)
                            <img src="{{ url($article->image_intro_path) }}" alt="blog post">
                        @endif --}}
                        <h2>{{ $article->title }}</h2>
                        <div class="meta-info">
                            <span>
                                <i class="ti-user"></i> Written By {{ $article->author ? $article->author->name : 'Transformasi' }}
                            </span>
                            <span>
                                <i class="ti-bookmark"></i> Category {{ $article->category ? $article->category->name : '-' }}
                            </span>
                        </div><!-- Meta Info -->
                        
                        <div>{!! $article->content !!}</div>
                        
                        @if ($article->getAttachmentIds())
                            <div class="mt-3">
                                <hr class="my-2">
                                @foreach ($article->getAttachmentIds() as $attachment_id)
                                    @if (!isset($attachments[$attachment_id]))
                                        @continue
                                    @endif
                                    <div class="alert alert-info mb-2">
                                        <a href="{{ route('home.attachment.download', $attachment_id) }}"><small><b><i class="fa fa-download"></i> &nbsp; {{ $attachments[$attachment_id]['title'] }} ({{ $attachments[$attachment_id]['download_count'] }} downloads)</b></small></a>
                                    </div>
                                @endforeach
                            </div>
                        @endif

                        <div class="share-wrap">
                            <h4>Share This Article</h4>
                            <ul class="share-icon">
                                <li><a href="https://www.facebook.com/sharer/sharer.php?u={{ url()->current() }}"><i class="ti-facebook"></i> Facebook</a></li>
                                <li><a href="https://twitter.com/share?url={{ url()->current() }}"><i class="ti-twitter"></i> Twitter</a></li>
                                <li><a href="ttps://www.linkedin.com/sharing/share-offsite/?url={{ url()->current() }}"><i class="ti-linkedin"></i> Linkedin</a></li>
                            </ul>
                        </div><!-- Share Wrap -->
                    </div>
                </div><!-- Blog Posts -->
                <div class="col-lg-3 sm-padding">
                    <div class="sidebar-wrap">
                        <div class="sidebar-widget mb-50">
                            <h4>Cari Artikel</h4>
                            <form action="{{ route('home.articles') }}" class="search-form">
                                <input type="text" name="q" class="form-control" placeholder="Type here">
                                <button class="search-btn"><i class="fa fa-search"></i></button>
                            </form>
                        </div>
                        <div class="sidebar-widget mb-50">
                            <h4>Kategori</h4>
                            <ul class="cat-list">
                                @foreach ($categories as $category)
                                    <li><a href="{{ route('home.articles.category', $category->id."-".$category->slug) }}">{{ $category->name }}</a><span>({{ $category->articles_count }})</span></li>
                                @endforeach
                            </ul>
                        </div><!-- Categories -->
                        <div class="sidebar-widget">
                            <h4>Recent Posts</h4>
                            <ul class="recent-posts">
                                @foreach ($latests as $latest)
                                    <li class="@if(!$latest->thumbnail_url) d-block pl-0 @endif">
                                        @if($latest->thumbnail_url)
                                            <img src="{{ url($latest->thumbnail_url) }}" alt="blog post">
                                        @endif
                                        <div>
                                            <h4><a href="#">{{ \Illuminate\Support\Str::limit($latest->title, 45) }}</a></h4>
                                            <span class="date"><i class="fa fa-clock-o"></i> {{ $latest->created_at->format('d F Y') }}</span>
                                        </div>
                                    </li>
                                @endforeach
                            </ul>
                        </div><!-- Recent Posts -->
                        <div class="sidebar-widget newsletter mb-50">
                            <form action="{{ route('home.newsletter-subscribe') }}" method="POST">
                                @csrf
                                <h4>Dapatkan informasi terbaru</h4>
                                <input type="email" name="email" class="form-control" placeholder="Masukkan Email Anda..." required autocomplete="off">
                                <div class="spacer-10"> </div>
                                <div class="text-center">
                                    <button class="default-btn bg-tersier m-auto w-100" type="submit">                                        
                                        Kirim
                                    </button>
                                </div>
                            </form>
                        </div>
                        <!-- Tag -->
                    </div><!-- /Sidebar Wrapper -->
                </div>
            </div>
        </div>
    </section><!-- /Blog Section -->
@endsection