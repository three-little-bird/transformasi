@extends('web.layouts.default')

@section('content')
    <div class="container" style="min-height: 250px;">
        <div class="text-center">
            <h2 class="my-5">Newsletter</h2>
            <div class="alert alert-success my-3">
                <p>{{ $message }}</p>
            </div>
        </div>
    </div>
@endsection
