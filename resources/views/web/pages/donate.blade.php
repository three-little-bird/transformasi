@extends('web.layouts.default', [
    'title' => "Donasi - Transformasi"
])

@section('before-body-end')
    <script>
        console.log('ko');
        let checkForm = function () {
            if ($("#verification").is(':checked')) {
                $("#submitBtn").removeAttr('disabled');
            } else {
                $("#submitBtn").attr('disabled', true);
            }
        }

        $('form').on('change', function() {
            checkForm();
        });

        checkForm();
    </script>
@endsection

@section('content')
    <div class="pager-header">
        <div class="container">
            <div class="page-content">
                <h2>DONASI</h2>
            </div>
        </div>
    </div><!-- /Page Header -->

    <section class="donate-section padding bg-grey">
        <div class="container">
            <form action="{{ route('home.donate') }}" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="row donate-wrap bg-white">
                    <div class="col-12 xs-padding">
                        
                    </div>
                    <div class="col-12 xs-padding">
                        <div class="donate-form">
                            <div class="section-heading text-center mb-40">
                                <h2>Ayo Berdonasi</h2>
                                <span class="heading-border"></span>
                            </div>
                            <p>
                                Anda dapat mendukung kegiatan kami dengan menyalurkan bantuan berupa dana melalui: 
                            </p>
                            <h3>
                                Account BRI Cabang Kebayoran Baru 
                            </h3>
                            <p>
                                <b>Yay Transformasi (IDR) a/c: 019301003039302</b>
                            </p>
                            <h3>
                                Account BRI Cabang Kebayoran Baru 
                            </h3>
                            <p>
                                <b>Yay Transformasi (Dollar) a/c: 019302000274307</b>
                            </p>
                            <div class="spacer-20"></div>
                            <div class="section-heading text-center mb-40">
                                <h2>Form Donasi</h2>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-6">
                                    <input type="text" id="name" name="name" class="form-control" placeholder="Nama" required>
                                </div>
                                <div class="col-md-6">
                                    <input type="email" id="email" name="email" class="form-control" placeholder="Email" required>
                                </div>
                            </div>
                            <!-- <div class="form-group row">
                                <div class="col-md-12">
                                    <input type="email" id="email" name="email" class="form-control" placeholder="Email" required>
                                </div>
                            </div> -->
                            <div class="form-group row">
                                <div class="col-md-6">
                                    <input type="text" id="amount" name="amount" class="form-control" placeholder="Jumlah Dalam Rupiah" required>
                                </div>
                                <div class="col-md-6">
                                    <input type="text" id="phone" name="phone" class="form-control" placeholder="Kontak Pendonasi (Nomor Handphone)" required>
                                </div>
                            </div>
                            <!-- <div class="form-group row">
                                <div class="col-md-12">
                                    <input type="text" id="phone" name="phone" class="form-control" placeholder="Kontak Pendonasi (Nomor Handphone)" required>
                                </div>
                            </div> -->
                            <div class="form-group row">
                                <div class="col-md-12">
                                    <label class="font-weight-bold">Upload Bukti Transfer (Format gambar)</label>
                                    <input type="file" id="file" name="file" class="form-control" placeholder="Kontak Pendonasi (Nomor Handphone)" accept=".jpg,.jpeg,.png" required>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-12 row">
                                    <div class="col-md-12">
                                        <input type="checkbox" id="newsletter" name="newsletter" value="1">
                                        <label for="newsletter"> Dapatkan berita transformasi terbaru &amp; laporan tahunan
                                            terbaru</label><br>
                                    </div>
                                    <div class="col-md-12">
                                        <input type="checkbox" id="verification" name="verification" value="1" required>
                                        <label for="verification"> Dengan ini saya menyetujui bahwa uang yang
                                            didonasikan bukan hasil dari aktifitas ilegal</label><br>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <button id="submitBtn" class="default-btn bg-tersier d-block text-center w-100" type="submit">
                                            Kirim
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </section><!-- /Contact Section -->
@endsection