@extends('web.layouts.default')

@section('meta')
    <meta name="author" content="transformasi">
    @if (getLocale() == 'id-ID')
        <meta name="description" content="Transformasi adalah wadah jaringan think tank yang melibatkan para pengambil kebijakan, akademisi, dan publik dalam melakukan investigasi permasalahan publik">
    @else
        <meta name="description" content="Transformasi is a think tank network forum that involves policy makers, academics, and the public in investigating public problems">
    @endif
@endsection

@section('css')
    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css"/>
@endsection

@section('before-body-end')
    <div id="fb-root"></div>
    <script async defer crossorigin="anonymous" src="https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v11.0&appId=439981252789383&autoLogAppEvents=1" nonce="58PDzfoK"></script>
    <script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
    <script>
        $(document).ready(function(){
            $('.slider-main').slick({
                autoplay: true,
                autoplaySpeed: 8000,
                arrows: false
            });
        });
    </script>
@endsection

@section('content')
    <section class="slider-section">
        <div class="slider-wrapper">
            <div class="slider-main d-md-none">
                @foreach ($carousels as $carousel)
                    <div class="slider-content" style="background-image: url({{ asset($carousel->bg_path) }})">
                        <div class="slider-caption">
                            <div class="container slider-text">
                                <h1 class="wow cssanimation leFadeInRight sequence">{{ $carousel->title }}</h1>
                                <p class="wow cssanimation fadeInTop" data-wow-delay="1s">
                                <p class="text-center">{{ $carousel->description }}</p>
                                <div class="">
                                    <a href="{{ $carousel->link }}" class="default-btn bg-tersier d-inline-block px-3 wow cssanimation fadeInBottom"
                                                data-wow-delay="0.8s">
                                                {{ __('main.read-more') }}
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
            <div class="d-none d-md-block">
                <div id="main-slider" class="nivoSlider">
                    @foreach ($carousels as $carousel)
                        <img src="{{ asset($carousel->bg_path) }}" alt="" title="#slider-caption-{{ $carousel->id }}" />
                    @endforeach
                </div><!-- /#main-slider -->
    
                @foreach ($carousels as $carousel)
                    <div id="slider-caption-{{ $carousel->id }}" class="nivo-html-caption slider-caption">
                        <div class="display-table">
                            <div class="table-cell">
                                <div class="container">
                                    <div class="slider-text">
                                        <h1 class="wow cssanimation leFadeInRight sequence">{{ $carousel->title }}</h1>
                                        <p class="wow cssanimation fadeInTop" data-wow-delay="1s">
                                        <p class="text-center">{{ $carousel->description }}</p>
                                        <a href="{{ $carousel->link }}" class="default-btn bg-tersier d-inline-block px-3 wow cssanimation fadeInBottom"
                                            data-wow-delay="0.8s">
                                            {{ __('main.read-more') }}
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </section><!-- /#slider-Section -->

    <section class="blog-section lp-blog-section section-news-event padding">
        <div class="container">
            <div class="section-heading text-center mb-40">
                <h2 class="text-white">{{ __('main.newly-news-title') }}</h2>
                <span class="heading-border"></span>
            </div>
            <div class="row">
                <div class="col-lg-12 xs-padding">
                    <div id="event-carousel" class="blog-items grid-list">
                        @foreach ($articles as $article)
                            <div class="mb-40">
                                <div class="blog-post">
                                    <img class="br-top"src="{{ $article->thumbnail_url ? asset($article->thumbnail_url) : config('app.default_image') }}"
                                        alt="blog post">
                                    <div class="blog-content br-bottom h-200">
                                        <div class="h-80">
                                            <span class="lp-blog-date"><i class="fa fa-clock-o"></i> {{ $article->created_at->format('d F Y') }}</span>
                                            <h3 class="lp-blog-title"><a href="{{ route('home.article', $article->slug) }}">{{ $article->title }}</a></h3>
                                        </div>
                                        <div class="">
                                            <a href="{{ route('home.article', $article->slug) }}" class="post-meta">{{ __('main.read-more') }}<i class="fa fa-arrow-right ml-2"></i></a>
                                        </div>
                                    </div>
                                    
                                </div>
                            </div>
                        @endforeach
                    </div>
                    <div class="text-center">
                        <a href="{{ route('home.articles') }}" class="default-btn bg-tersier d-inline-block px-3">
                            {{ __('main.view-all-article') }}<i class="fa fa-arrow-circle-right ml-2"></i>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="team-section bg-grey">
        <div class="container">
            <div class="team-wrapper row">
                <div class="col-lg-6 ">
                    <div class=" h-100 d-flex justify-content-center row padding-30">
                       <img src="{{ __('main.join-img') }}" alt="" class="join-img">
                    </div>
                </div>
                <div class="col-lg-6 sm-padding">
                    <div class="team-content">
                        <h2 class="color-tersier">
                            {{ __('main.join-us-title') }}
                        </h2>
                        <p class="text-justify">
                            {{ __('main.join-us-desc') }}
                        </p>
                        <p class="text-justify">
                            {{ __('main.join-us-subdesc') }}
                        </p>
                        <a href="{{ url('/ikut-terlibat') }}" class="default-btn bg-tersier wow cssanimation fadeInBottom"
                                data-wow-delay="0.8s">
                            {{ __('main.join') }}
                        </a>
                    </div>
                </div>
            </div>
        <!-- </div> -->
    </section><!-- /Team Section -->

    <section class="causes-section publication-section padding">
        <div class="container">
            <div class="section-heading text-center mb-40">
                <h2 class="text-white">{{ __('main.recent-publication') }}</h2>
                <span class="heading-border"></span>
            </div><!-- /Section Heading -->
            <div class="causes-wrap mb-40 row">
                @foreach ($publications as $publication)
                    <div class="col-md-3 xs-padding">
                        <div class="causes-content">
                            <div class="causes-thumb">
                                <img class="" src="{{ $publication->image_url ? asset($publication->image_url) : config('app.default_image') }}" alt="causes">
                                <a href="#" class="donate-btn bg-tersier">{{ __('main.read-more') }} <i class="fa fa-arrow-circle-right ml-2"></i></i></a>
                            </div>
                            <div class="causes-details h-130">
                                <h4>{{ $publication->title }}</h4>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
            <div class="text-center">
                <a href="{{ route('home.publication') }}" class="default-btn bg-tersier d-inline-block px-3">
                    {{ __('main.view-all-publication') }} <i class="fa fa-arrow-circle-right ml-2"></i>
                </a>
            </div>
        </div>
    </section><!-- /Causes Section -->
    <!-- <section class="socmed-section bg-grey circle shape padding">
        <div class="container-fluid">
            <div class="section-heading text-center mb-40">
                <h2>{{ __('main.social-media-title') }}</h2>
                <span class="heading-border"></span>
                {{-- <p>{{ __('main.social-media-desc') }}</p> --}}
            <div class="socmed-content row padding">
                <div class="col-lg-3 socmed">
                    <h3>Twitter</h3>
                    <div class="twitter">
                        <a class="twitter-timeline text-center" data-width="500" data-height="315" href="https://twitter.com/transformasi_id?ref_src=twsrc%5Etfw">Tweets by transformasi_id</a> 
                                <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
                    </div>
                </div>
                <div class="col-lg-3 socmed">
                    <h3>Facebook</h3>
                    <div class="facebook">
                        <div class="fb-page" data-href="https://www.facebook.com/pusattransformasikebijakanpublik" data-tabs="timeline" data-width="500" data-height="315" data-small-header="true" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/pusattransformasikebijakanpublik" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/pusattransformasikebijakanpublik">Pusat Transformasi Kebijakan Publik</a></blockquote></div>
                    </div>
                </div>
                <div class="col-lg-6 socmed video-container w-100 ">
                    <h3>Youtube</h3>
                    <div class="youtube">
                        {!! $video->embedcode !!}
                    </div>
                </div>
            </div>
        </div>
    </section> -->
    <section class="padding bg-grey circle shape">
        <div class="container">
        <div class="section-heading text-center mb-40">
                <h2 class="">
                    {{ __('main.social-media-title') }}
                </h2>
                <span class="heading-border"></span>
            </div><!-- /Section Heading -->
            <div class="row">
                <div class="col-md-4">
                    <nav>
                        <div class="nav nav-tabs nav-tabs nav-fill mt-0 pt-0 row" id="nav-tab" role="tablist">
                            <a class="nav-item nav-link active" id="nav-facebok-tab" data-toggle="tab" href="#facebook" role="tab" aria-controls="nav-facebook" aria-selected="false">FACEBOOK</a>
                            <a class="nav-item nav-link " id="nav-twitter-tab" data-toggle="tab" href="#twitter" role="tab" aria-controls="nav-twitter" aria-selected="false">TWITTER</a>
                        </div>
                    </nav>
                    <div class="tab-content px-sm-0" id="nav-tabContent">
                        <div class="tab-pane fade  active show" id="facebook" role="tabpanel" aria-labelledby="nav-home-tab">
                            <div class="about-section">
                                <div class="row about-wrap">
                                    <div class="col-md-12 xs-padding">
                                        <div class="about-content mb-20">
                                            <div class="section-heading text-center mb-40">
                                                <div class="fb-page" data-href="https://www.facebook.com/pusattransformasikebijakanpublik" data-tabs="timeline" data-width="500" data-height="353" data-small-header="true" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/pusattransformasikebijakanpublik" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/pusattransformasikebijakanpublik">Pusat Transformasi Kebijakan Publik</a></blockquote></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade" id="twitter" role="tabpanel" aria-labelledby="nav-profile-tab">
                            <div class="about-section">
                                <div class="row about-wrap">
                                    <div class="col-md-12 xs-padding">
                                        <div class="about-content mb-20">
                                            <div class="section-heading text-center mb-40">
                                                <a class="twitter-timeline" data-width="500" data-height="353" href="https://twitter.com/transformasi_id?ref_src=twsrc%5Etfw">Tweets by transformasi_id</a> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-8">
                    <h2 class="text-center">
                            YOUTUBE
                    </h2>
                    <div class="spacer-half"></div>
                    <div id="youtube-wrapper" class="section-heading text-center mb-40">
                        
                        {!! $video->embedcode !!}
                    </div>
                </div>
            </div>
        </div>
    </section>  



    <div class="sponsor-section bd-bottom">
        <div class="container">
            <ul id="sponsor-carousel" class="sponsor-items owl-carousel">
                <li class="sponsor-item">
                    <img src="https://www.transformasi.org/images/PF.jpg" alt="sponsor-image">
                </li>
                <li class="sponsor-item">
                    <img src="https://www.transformasi.org/images/banners/Rajawali_Foundation.jpg" alt="sponsor-image">
                </li>
                <li class="sponsor-item">
                    <img src="https://www.transformasi.org/images/gallery/Logo/ICCTF.PNG" alt="sponsor-image">
                </li>
                <li class="sponsor-item">
                    <img src="https://www.transformasi.org/images/USAID_id.png" alt="sponsor-image">
                </li>
                <li class="sponsor-item">
                    <img src="https://www.transformasi.org/images/gallery/Logo/CLUA.png" alt="sponsor-image">
                </li>
                <li class="sponsor-item">
                    <img src="https://www.transformasi.org/images/PF.jpg" alt="sponsor-image">
                </li>
                <li class="sponsor-item">
                    <img src="https://www.transformasi.org/images/banners/Rajawali_Foundation.jpg" alt="sponsor-image">
                </li>
                <li class="sponsor-item">
                    <img src="https://www.transformasi.org/images/gallery/Logo/ICCTF.PNG" alt="sponsor-image">
                </li>
                <li class="sponsor-item">
                    <img src="https://www.transformasi.org/images/USAID_id.png" alt="sponsor-image">
                </li>
                <li class="sponsor-item">
                    <img src="https://www.transformasi.org/images/gallery/Logo/CLUA.png" alt="sponsor-image">
                </li>
            </ul>
        </div>
    </div><!-- ./Sponsor Section -->
@endsection
