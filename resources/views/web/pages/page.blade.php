@if ($page->layout == 'page')
    @php
        $pgUsed = 'page';
    @endphp
@else
    @php
        $pgUsed = 'default';
    @endphp
@endif

@section('meta')
    @if ($page->meta())
        @if (isset($page->meta()['author']) && $page->meta()['author'])
            <meta name="author" content="{{ $page->meta()['author'] ? $page->meta()['author'] : $page->author->name }}" />
        @endif
        @if (isset($page->meta()['description']) && $page->meta()['description'])
            <meta name="description" content="{{ $page->meta()['description'] }}" />
        @endif
        @if (isset($page->meta()['key']) && $page->meta()['key'])
            <meta name="keywords" content="{{ $page->meta()['key'] }}" />
        @endif
        @if (isset($page->meta()['robots']) && $page->meta()['robots'])
            <meta name="robots" content="{{ $page->meta()['robots'] }}" />
        @endif
    @endif
@endsection

@extends('web.layouts.'.$pgUsed)

@section('title')
    {{ $page->title }}
@endsection

@section('content')
    {!! $page->content !!}
@endsection