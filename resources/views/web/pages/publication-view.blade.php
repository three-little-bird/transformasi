@extends('web.layouts.default', [
    'title' => $publication->title." - Transformasi"
])

@section('content')
    <section class="blog-section bg-grey padding">
        <div class="container">
            <div class="row">
                <div class="col-lg-9 sm-padding">
                    <div class="blog-items right-sidebar single-post">
                        <h2>{{ $publication->title }}</h2>
                        
                        <div>{!! $publication->description !!}</div>
                        
                        @if ($publication->getAttachmentIds())
                            <div class="mt-3">
                                <hr class="my-2">
                                @foreach ($publication->getAttachmentIds() as $attachment_id)
                                    @if (!isset($attachments[$attachment_id]))
                                        @continue
                                    @endif
                                    <div class="alert alert-info mb-2">
                                        <a href="{{ route('home.attachment.download', $attachment_id) }}"><small><b><i class="fa fa-download"></i> &nbsp; {{ $attachments[$attachment_id]['title'] }} ({{ $attachments[$attachment_id]['download_count'] }} downloads)</b></small></a>
                                    </div>
                                @endforeach
                            </div>
                        @endif

                        <div class="share-wrap">
                            <h4>Share This publication</h4>
                            <ul class="share-icon">
                                <li><a href="https://www.facebook.com/sharer/sharer.php?u={{ url()->current() }}"><i class="ti-facebook"></i> Facebook</a></li>
                                <li><a href="https://twitter.com/share?url={{ url()->current() }}"><i class="ti-twitter"></i> Twitter</a></li>
                                <li><a href="ttps://www.linkedin.com/sharing/share-offsite/?url={{ url()->current() }}"><i class="ti-linkedin"></i> Linkedin</a></li>
                            </ul>
                        </div><!-- Share Wrap -->
                    </div>
                </div>
                
                <div class="col-lg-3 sm-padding">
                    <div class="sidebar-wrap">
                        <div class="card card-body">
                            <b>Info Publikasi</b>
                            <br>
                            
                            @if ($publication->topic)
                                <div>
                                    Topik : {{ $publication->topic->name }}
                                </div>
                            @endif
                            <div>
                                Tipe : {{ $typeText[$publication->type] }}
                            </div>
                            @if ($publication->author)
                                <div>
                                    Author : {{ $publication->author }}
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section><!-- /Blog Section -->
@endsection