<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('settings', function (Blueprint $table) {
            $table->id();
            $table->string('key');
            $table->string('value')->nullable();
            $table->string('secondary_value')->nullable();
            $table->timestamps();
        });

        \DB::table('settings')
            ->insert([
                [
                    'key' => 'home_video_id',
                    'created_at' => now(),
                    'updated_at' => now()
                ],
                [
                    'key' => 'home_video_en',
                    'created_at' => now(),
                    'updated_at' => now()
                ],
            ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('settings');
    }
}
