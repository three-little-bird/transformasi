<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Models\Blast;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

require __DIR__.'/admin.php';

Route::middleware('lang')->group(function ()
{
    Route::get('/', [HomeController::class, 'index'])->name('home');
    Route::get('/articles', [HomeController::class, 'articles'])->name('home.articles');
    Route::get('/articles/category/{slug}', [HomeController::class, 'articles'])->name('home.articles.category');
    Route::get('/article/{slug}', [HomeController::class, 'article'])->name('home.article');
    Route::get('/publications', [HomeController::class, 'publications'])->name('home.publication');
    Route::get('/publications/view/{id}', [HomeController::class, 'publicationView'])->name('home.publication.view');
    Route::get('/attachment/download/{id}', [HomeController::class, 'attachmentDownload'])->name('home.attachment.download');
    Route::get('/galleries/{id?}', [HomeController::class, 'galleries'])->name('home.galleries');
    Route::get('/galleries-photo/download/{id}', [HomeController::class, 'photoDownload'])->name('home.photo.download');
    Route::get('/videos/{year?}', [HomeController::class, 'videos'])->name('home.videos');
    Route::get('/donate', [HomeController::class, 'donate'])->name('home.donate');
    Route::post('/donate', [HomeController::class, 'donateAction']);
    Route::get('/newsletter/subscribe', [HomeController::class, 'mailingSubscribe'])->name('home.newsletter-subscribe');
    Route::post('/newsletter/subscribe', [HomeController::class, 'mailingSubscribeAction']);
    Route::get('/blast', function ()
    {
        $blast = Blast::first();

        return view('emails.default', [
            'content' => $blast->content
        ]);
    });
    Route::get('/{slug}', [HomeController::class, 'page'])->name('home.page');

});