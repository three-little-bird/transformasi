<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Admin\AuthController;
use App\Http\Controllers\Admin\ArticleController;
use App\Http\Controllers\Admin\AttachmentController;
use App\Http\Controllers\Admin\CategoryController;
use App\Http\Controllers\Admin\DashboardController;
use App\Http\Controllers\Admin\DonationController;
use App\Http\Controllers\Admin\GalleryController;
use App\Http\Controllers\Admin\HomeCarouselController;
use App\Http\Controllers\Admin\MailingController;
use App\Http\Controllers\Admin\MenuController;
use App\Http\Controllers\Admin\PageController;
use App\Http\Controllers\Admin\PublicationController;
use App\Http\Controllers\Admin\VideoController;
use App\Http\Controllers\Admin\VideoMainController;

Route::get('admin', function() { return redirect()->route('admin.login'); })->name('admin');
Route::get('admin/login', [AuthController::class, 'loginForm'])->name('admin.login')->middleware('guest');
Route::post('admin/login', [AuthController::class, 'login']);

Route::prefix('admin')->middleware('auth')->group(function ()
{
    Route::get('admin/logout', [AuthController::class, 'logout'])->name('admin.logout');

    Route::get('dashboard', [DashboardController::class, 'index'])->name('admin.dashboard');

    Route::get('article', [ArticleController::class, 'index'])->name('admin.article');
    Route::get('article/add', [ArticleController::class, 'add'])->name('admin.article.add');
    Route::post('article/add', [ArticleController::class, 'addAction']);
    Route::get('article/edit/{id}', [ArticleController::class, 'edit'])->name('admin.article.edit');
    Route::post('article/edit/{id}', [ArticleController::class, 'editAction']);
    Route::get('article/delete/{id}', [ArticleController::class, 'deleteAction'])->name('admin.article.delete');

    Route::get('categories', [CategoryController::class, 'index'])->name('admin.categories');
    Route::get('categories/add', [CategoryController::class, 'add'])->name('admin.categories.add');
    Route::post('categories/add', [CategoryController::class, 'addAction']);
    Route::get('categories/edit/{id}', [CategoryController::class, 'edit'])->name('admin.categories.edit');
    Route::post('categories/edit/{id}', [CategoryController::class, 'editAction']);
    Route::get('categories/delete/{id}', [CategoryController::class, 'deleteAction'])->name('admin.categories.delete');

    Route::get('galleries', [GalleryController::class, 'index'])->name('admin.galleries');
    Route::get('galleries/add', [GalleryController::class, 'add'])->name('admin.galleries.add');
    Route::post('galleries/add', [GalleryController::class, 'addAction']);
    Route::get('galleries/edit/{id}', [GalleryController::class, 'edit'])->name('admin.galleries.edit');
    Route::post('galleries/edit/{id}', [GalleryController::class, 'editAction']);
    Route::get('galleries/delete/{id}', [GalleryController::class, 'deleteAction'])->name('admin.galleries.delete');

    Route::get('galleries/photos', [GalleryController::class, 'photo'])->name('admin.galleries-photo');
    Route::get('galleries/photos/add', [GalleryController::class, 'photoAdd'])->name('admin.galleries-photo.add');
    Route::post('galleries/photos/add', [GalleryController::class, 'photoAddAction']);
    Route::get('galleries/photos/edit/{id}', [GalleryController::class, 'photoEdit'])->name('admin.galleries-photo.edit');
    Route::post('galleries/photos/edit/{id}', [GalleryController::class, 'photoEditAction']);
    Route::get('galleries/photos/delete/{id}', [GalleryController::class, 'photoDeleteAction'])->name('admin.galleries-photo.delete');

    Route::get('videos', [VideoController::class, 'index'])->name('admin.videos');
    Route::get('videos/add', [VideoController::class, 'add'])->name('admin.videos.add');
    Route::post('videos/add', [VideoController::class, 'addAction']);
    Route::get('videos/edit/{id}', [VideoController::class, 'edit'])->name('admin.videos.edit');
    Route::post('videos/edit/{id}', [VideoController::class, 'editAction']);
    Route::get('videos/delete/{id}', [VideoController::class, 'deleteAction'])->name('admin.videos.delete');

    Route::get('publications', [PublicationController::class, 'index'])->name('admin.publications');
    Route::get('publications/add', [PublicationController::class, 'add'])->name('admin.publications.add');
    Route::post('publications/add', [PublicationController::class, 'addAction']);
    Route::get('publications/edit/{id}', [PublicationController::class, 'edit'])->name('admin.publications.edit');
    Route::post('publications/edit/{id}', [PublicationController::class, 'editAction']);
    Route::get('publications/delete/{id}', [PublicationController::class, 'deleteAction'])->name('admin.publications.delete');

    Route::get('publications/topics', [PublicationController::class, 'topic'])->name('admin.publications.topics');
    Route::get('publications/topics/add', [PublicationController::class, 'topicAdd'])->name('admin.publications.topics.add');
    Route::post('publications/topics/add', [PublicationController::class, 'topicAddAction']);
    Route::get('publications/topics/edit/{id}', [PublicationController::class, 'topicEdit'])->name('admin.publications.topics.edit');
    Route::post('publications/topics/edit/{id}', [PublicationController::class, 'topicEditAction']);
    Route::get('publications/topics/delete/{id}', [PublicationController::class, 'topicDeleteAction'])->name('admin.publications.topics.delete');

    Route::get('home-carousels', [HomeCarouselController::class, 'index'])->name('admin.home-carousels');
    Route::get('home-carousels/add', [HomeCarouselController::class, 'add'])->name('admin.home-carousels.add');
    Route::post('home-carousels/add', [HomeCarouselController::class, 'addAction']);
    Route::get('home-carousels/edit/{id}', [HomeCarouselController::class, 'edit'])->name('admin.home-carousels.edit');
    Route::post('home-carousels/edit/{id}', [HomeCarouselController::class, 'editAction']);
    Route::get('home-carousels/delete/{id}', [HomeCarouselController::class, 'deleteAction'])->name('admin.home-carousels.delete');

    Route::get('main-videos', [VideoMainController::class, 'index'])->name('admin.main-videos');
    Route::post('main-videos/update/{lang}', [VideoMainController::class, 'update'])->name('admin.main-videos.update');

    Route::get('attachments', [AttachmentController::class, 'index'])->name('admin.attachments');
    Route::get('attachments/add', [AttachmentController::class, 'add'])->name('admin.attachments.add');
    Route::post('attachments/add', [AttachmentController::class, 'addAction']);
    Route::get('attachments/edit/{id}', [AttachmentController::class, 'edit'])->name('admin.attachments.edit');
    Route::post('attachments/edit/{id}', [AttachmentController::class, 'editAction']);
    Route::get('attachments/delete/{id}', [AttachmentController::class, 'deleteAction'])->name('admin.attachments.delete');

    Route::get('pages', [PageController::class, 'index'])->name('admin.pages');
    Route::get('pages/add', [PageController::class, 'add'])->name('admin.pages.add');
    Route::post('pages/add', [PageController::class, 'addAction']);
    Route::get('pages/edit/{id}', [PageController::class, 'edit'])->name('admin.pages.edit');
    Route::post('pages/edit/{id}', [PageController::class, 'editAction']);
    Route::get('pages/delete/{id}', [PageController::class, 'deleteAction'])->name('admin.pages.delete');

    Route::get('menu-manager', [MenuController::class, 'index'])->name('admin.menu-manager');
    Route::get('menu-manager/api/update', [MenuController::class, 'apiUpdate'])->name('admin.menu-manager.api.update');

    Route::get('mailing/blast', [MailingController::class, 'create'])->name('admin.mailing.create');
    Route::post('mailing/blast', [MailingController::class, 'blast'])->name('admin.mailing.blast');
    Route::get('mailing/lists', [MailingController::class, 'lists'])->name('admin.mailing.lists');
    Route::get('mailing/lists/add', [MailingController::class, 'addList'])->name('admin.mailing.lists.add');
    Route::post('mailing/lists/add', [MailingController::class, 'addListAction']);
    Route::get('mailing/lists/edit/{id}', [MailingController::class, 'editList'])->name('admin.mailing.lists.edit');
    Route::post('mailing/lists/edit/{id}', [MailingController::class, 'editListAction']);
    Route::get('mailing/lists/delete/{id}', [MailingController::class, 'deleteList'])->name('admin.mailing.lists.delete');
    Route::get('mailing/history', [MailingController::class, 'history'])->name('admin.mailing.history');

    Route::get('donations', [DonationController::class, 'index'])->name('admin.donations');
});