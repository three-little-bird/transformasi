<?php

use Illuminate\Foundation\Inspiring;
use Illuminate\Support\Facades\Artisan;

/*
|--------------------------------------------------------------------------
| Console Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of your Closure based console
| commands. Each Closure is bound to a command instance allowing a
| simple approach to interacting with each command's IO methods.
|
*/

Artisan::command('inspire', function () {
    $this->comment(Inspiring::quote());
})->purpose('Display an inspiring quote');

Artisan::command('migrate:article', function () {
    \App\Models\Article::truncate();
    $oldArticles = \DB::connection('old')
        ->table('web_content')
        ->orderBy('id', 'ASC')
        ->get();

    foreach ($oldArticles as $oldArticle) {
        $image = json_decode($oldArticle->images, true);
        if (strlen(@$image['image_intro']) > 255) {
            $image['image_intro'] = null;
        }

        // --
        $oldArticle->introtext = str_replace('src="images/', 'src="/images/', $oldArticle->introtext);
        $thumbnail_url = null;
        if (@$image['image_intro']) {
            $this->info($image['image_intro']);
            try {
                $img = \Image::make(public_path($image['image_intro']));
                $img->resize(720, null, function ($constraint) {
                    $constraint->aspectRatio();
                    // $constraint->upsize();
                });
                $img->crop(720, 405);
                $thumbnail_url = 'images/thumbs/' . basename($image['image_intro']);
    
                if(!\File::exists(public_path('images/thumbs'))) {
                    \File::makeDirectory(public_path('images/thumbs'), $mode = 0777, true, true);
                }
                $img->save(public_path($thumbnail_url));
            } catch (\Exception $e) {
                $image['image_intro'] = null;
                $thumbnail_url = null;
                $this->error('Fail to load image');
            }
        }

        $newArticle = new \App\Models\Article();
        $newArticle->id = $oldArticle->id;
        $newArticle->title = $oldArticle->title;
        $newArticle->slug = $oldArticle->alias;
        $newArticle->content = $oldArticle->introtext;
        $newArticle->image_intro_path = @$image['image_intro'] ? $image['image_intro'] : null;
        $newArticle->thumbnail_url = $thumbnail_url;
        $newArticle->category_id = $oldArticle->catid;
        $newArticle->state = $oldArticle->state;
        $newArticle->created_by_id = $oldArticle->created_by;
        $newArticle->modified_by_id = $oldArticle->modified_by;
        $newArticle->attachments = $oldArticle->attachments;
        $newArticle->metadata = $oldArticle->metadata;
        $newArticle->hits = $oldArticle->hits;
        $newArticle->lang = $oldArticle->language;
        $newArticle->created_at = $oldArticle->created;
        $newArticle->updated_at = $oldArticle->modified > '2000-01-01 00:00:00' ? $oldArticle->modified : $oldArticle->created;
        $newArticle->save();
    }

    $statement = "ALTER TABLE articles AUTO_INCREMENT = ".($oldArticle->id + 1).";";
    \DB::unprepared($statement);
})->purpose('Display an inspiring quote');

Artisan::command('migrate:category', function () {
    \App\Models\Category::truncate();
    $oldCategories = \DB::connection('old')
        ->table('web_categories')
        ->orderBy('id', 'ASC')
        ->get();

    foreach ($oldCategories as $oldCategory) {
        $newCategory = new \App\Models\Category();
        $newCategory->id = $oldCategory->id;
        $newCategory->name = $oldCategory->title;
        $newCategory->slug = $oldCategory->alias;
        $newCategory->level = $oldCategory->level;
        $newCategory->path = $oldCategory->path;
        $newCategory->parent_id = $oldCategory->parent_id != 0 ? $oldCategory->parent_id : null;
        $newCategory->lang = $oldCategory->language;
        $newCategory->created_by_id = $oldCategory->created_user_id;
        $newCategory->modified_by_id = $oldCategory->modified_user_id != 0 ? $oldCategory->modified_user_id : null;
        $newCategory->created_at = $oldCategory->created_time;
        $newCategory->updated_at = $oldCategory->modified_time > '2000-01-01 00:00:00' ? $oldCategory->modified_time : $oldCategory->created_time;
        $newCategory->save();
    }

    $statement = "ALTER TABLE categories AUTO_INCREMENT = ".($oldCategory->id + 1).";";
    \DB::unprepared($statement);
})->purpose('Display an inspiring quote');

Artisan::command('migrate:gallery', function () {
    \App\Models\Gallery::truncate();
    \App\Models\GalleryPhoto::truncate();
    $oldGalleries = \DB::connection('old')
        ->table('web_gallery_album')
        ->orderBy('id', 'ASC')
        ->get();
    $oldGalleryPhotos = \DB::connection('old')
        ->table('web_gallery_photo')
        ->orderBy('id', 'ASC')
        ->get();

    foreach ($oldGalleries as $oldGallery) {
        $newGallery = new \App\Models\Gallery();
        $newGallery->id = $oldGallery->id;
        $newGallery->title = $oldGallery->title;
        $newGallery->description = $oldGallery->description;
        $newGallery->lang = $oldGallery->language;
        $newGallery->state = $oldGallery->published;
        $newGallery->created_at = $oldGallery->publish_date;
        $newGallery->updated_at = $oldGallery->publish_date;
        $newGallery->save();
    }

    foreach ($oldGalleryPhotos as $oldGalleryPhoto) {
        $newGallery = new \App\Models\GalleryPhoto();
        $newGallery->id = $oldGalleryPhoto->id;
        $newGallery->gallery_id = $oldGalleryPhoto->album_id;
        $newGallery->title = $oldGalleryPhoto->title;
        $newGallery->description = $oldGalleryPhoto->description;
        $newGallery->image_path = $oldGalleryPhoto->image_url;
        $newGallery->lang = $oldGalleryPhoto->language;
        $newGallery->created_at = now();
        $newGallery->updated_at = now();
        $newGallery->save();
    }

    $statement = "ALTER TABLE galleries AUTO_INCREMENT = ".($oldGallery->id + 1).";";
    \DB::unprepared($statement);

    $statement = "ALTER TABLE gallery_photos AUTO_INCREMENT = ".($oldGalleryPhoto->id + 1).";";
    \DB::unprepared($statement);
})->purpose('Display an inspiring quote');

Artisan::command('migrate:video', function () {
    \App\Models\Video::truncate();
    $oldVideos = \DB::connection('old')
        ->table('web_video')
        ->orderBy('id', 'ASC')
        ->get();

    foreach ($oldVideos as $oldVideo) {
        $newVideo = new \App\Models\Video();
        $newVideo->id = $oldVideo->id;
        $newVideo->title = $oldVideo->title;
        $newVideo->embedcode = $oldVideo->embedcode;
        $newVideo->description = $oldVideo->description;
        $newVideo->lang = $oldVideo->language;
        $newVideo->is_published = $oldVideo->published;
        $newVideo->created_at = $oldVideo->created;
        $newVideo->updated_at = $oldVideo->checked_out_time > '2000-01-01 00:00:00' ? $oldVideo->checked_out_time : $oldVideo->created;
        $newVideo->save();
    }

    $statement = "ALTER TABLE videos AUTO_INCREMENT = ".($oldVideo->id + 1).";";
    \DB::unprepared($statement);
})->purpose('Display an inspiring quote');

Artisan::command('migrate:publication', function () {
    \App\Models\Topic::truncate();
    \App\Models\Publication::truncate();
    $topics = \App\Models\Topic::insert([
        [
            'name' => 'Kelautan dan Perikanan',
            'lang' => 'id-ID',
            'created_at' => now(),
            'updated_at' => now()
        ],
        [
            'name' => 'Lapangan Pekerjaan',
            'lang' => 'id-ID',
            'created_at' => now(),
            'updated_at' => now()
        ],
        [
            'name' => 'Ekonomi',
            'lang' => 'id-ID',
            'created_at' => now(),
            'updated_at' => now()
        ],
        [
            'name' => 'Pengelolaan Sampah',
            'lang' => 'id-ID',
            'created_at' => now(),
            'updated_at' => now()
        ],
        [
            'name' => 'Marine and Fisheries',
            'lang' => 'en-GB',
            'created_at' => now(),
            'updated_at' => now()
        ],
        [
            'name' => 'Economic',
            'lang' => 'en-GB',
            'created_at' => now(),
            'updated_at' => now()
        ]
    ]);

    $olds = \DB::connection('old')
        ->table('web_data_data')
        ->orderBy('id', 'ASC')
        ->get();

    foreach ($olds as $old) {
        $new = new \App\Models\Publication();
        // $new->id = $old->id;
        $new->type = 'data';
        $new->topic_id = null;
        $new->title = $old->title;
        $new->description = $old->description;
        $new->author = null;
        $new->publish_date = $old->publish_date;
        $new->image_url = $old->image;
        $new->lang = $old->language;
        $new->attachments = $old->attachments;
        $new->is_published = $old->published;
        $new->created_at = $old->publish_date.' 00:00:00';
        $new->updated_at = $old->checked_out_time > '2000-01-01 00:00:00' ? $old->checked_out_time : $new->created_at;
        $new->save();
    }

    $olds = \DB::connection('old')
        ->table('web_data_infographic')
        ->orderBy('id', 'ASC')
        ->get();

    foreach ($olds as $old) {
        $new = new \App\Models\Publication();
        // $new->id = $old->id;
        $new->type = 'infographic';
        $new->topic_id = null;
        $new->title = $old->title;
        $new->description = $old->description;
        $new->author = null;
        $new->publish_date = $old->publish_date;
        $new->image_url = $old->image;
        $new->lang = $old->language;
        $new->attachments = $old->attachments;
        $new->is_published = $old->published;
        $new->created_at = $old->publish_date.' 00:00:00';
        $new->updated_at = $old->checked_out_time > '2000-01-01 00:00:00' ? $old->checked_out_time : $new->created_at;
        $new->save();
    }

    $olds = \DB::connection('old')
        ->table('web_publication')
        ->orderBy('id', 'ASC')
        ->get();

    foreach ($olds as $old) {
        $new = new \App\Models\Publication();
        // $new->id = $old->id;
        $new->type = $old->publication_type == 1 ? 'book' : 'research';
        $new->topic_id = null;
        $new->title = $old->title;
        $new->description = $old->description;
        $new->author = $old->authors;
        $new->publish_date = $old->publish_date;
        $new->image_url = $old->image_url;
        $new->lang = $old->language;
        $new->attachments = $old->attachments;
        $new->is_published = $old->published;
        $new->created_at = $old->publish_date.' 00:00:00';
        $new->updated_at = $old->checked_out_time > '2000-01-01 00:00:00' ? $old->checked_out_time : $new->created_at;
        $new->save();
    }

    $categoryMap = [95 => 1, 98 => 2, 242 => 3, 271 => 4, 84 => 5, 243 => 6];
    $olds = \DB::connection('old')
        ->table('web_content')
        ->whereIn('catid', array_keys($categoryMap))
        ->orderBy('id', 'ASC')
        ->get();

    foreach ($olds as $old) {
        $image = json_decode($old->images, true);
        if (strlen(@$image['image_intro']) > 255) {
            $this->info(@$image['image_intro']);
            $image['image_intro'] = null;
        }
        $old->introtext = str_replace('src="images/', 'src="/images/', $old->introtext);
        $new = new \App\Models\Publication();
        // $new->id = $old->id;
        $new->type = 'policy-brief';
        $new->topic_id = $categoryMap[$old->catid];
        $new->title = $old->title;
        $new->description = $old->introtext;
        $new->author = null;
        $new->publish_date = $old->created;
        $new->image_url = @$image['image_intro'] ? $image['image_intro'] : '';
        $new->lang = $old->language;
        $new->attachments = $old->attachments;
        $new->is_published = $old->state == 1 ? 1 : 0;
        $new->created_at = $old->created;
        $new->updated_at = $old->modified > '2000-01-01 00:00:00' ? $old->modified : $old->created;
        $new->save();
    }

    $statement = "ALTER TABLE publications AUTO_INCREMENT = ".($new->id + 1).";";
    \DB::unprepared($statement);
})->purpose('Display an inspiring quote');

Artisan::command('migrate:attachment', function () {
    \App\Models\Attachment::truncate();
    $old = \DB::connection('old')
        ->table('web_attachment')
        ->orderBy('id', 'ASC')
        ->get();

    foreach ($old as $old) {
        $new = new \App\Models\Attachment();
        $new->id = $old->id;
        $new->title = $old->title;
        $new->description = $old->description;
        $new->file_path = $old->file_url;
        $new->download_count = $old->downloadcount;
        $new->lang = $old->language;
        $new->is_published = $old->published == 1 ? 1 : 0;
        $new->created_at = $old->publish_date." 00:00:00";
        $new->updated_at = $old->checked_out_time > '2000-01-01 00:00:00' ? $old->checked_out_time : $new->created_at;
        $new->save();
    }

    $statement = "ALTER TABLE attachments AUTO_INCREMENT = ".($old->id + 1).";";
    \DB::unprepared($statement);
})->purpose('Display an inspiring quote');