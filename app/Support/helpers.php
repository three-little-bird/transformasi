<?php

if (!function_exists('getLocale')) {
    function getLocale()
    {
        $locale = \Illuminate\Support\Facades\App::currentLocale();
        return $locale == 'en' ? 'en-GB' : 'id-ID';
    }
}