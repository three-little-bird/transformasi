<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Topic extends Model
{
    use HasFactory;

    public function language()
    {
        if ($this->lang == "*") {
            $lang = 'All';
        } else if ($this->lang == "id-ID") {
            $lang = 'Bahasa Indonesia';
        } else if ($this->lang == "en-GB") {
            $lang = 'English';
        }

        return $lang;
    }
}
