<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
    use HasFactory;

    public function author()
    {
        return $this->belongsTo(User::class, 'created_by_id');
    }

    public function status()
    {
        if ($this->state == 1) {
            $status = 'Published';
        } else if ($this->state == 0) {
            $status = 'Unpublished';
        } else if ($this->state == -1) {
            $status = 'Trashed';
        } else if ($this->state == -2) {
            $status = 'Deleted';
        } else {
            $status = 'Unknown ('.$this->state.')';
        }

        return $status;
    }

    public function language()
    {
        if ($this->lang == "*") {
            $lang = 'All';
        } else if ($this->lang == "id-ID") {
            $lang = 'Bahasa Indonesia';
        } else if ($this->lang == "en-GB") {
            $lang = 'English';
        }

        return $lang;
    }

    public function meta()
    {
        $meta = json_decode($this->metadata, true);

        return $meta ? $meta : [];
    }

    public function getAttachmentIds() {
        $json = json_decode($this->attachments, true);
        $ids = [];

        if ($json) {
            foreach ($json as $data) {
                $ids[] = $data;
            }
        }

        return $ids;
    }
}
