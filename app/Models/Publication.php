<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Publication extends Model
{
    use HasFactory;

    public function topic()
    {
        return $this->belongsTo(Topic::class);
    }

    public function language()
    {
        if ($this->lang == "*") {
            $lang = 'All';
        } else if ($this->lang == "id-ID") {
            $lang = 'Bahasa Indonesia';
        } else if ($this->lang == "en-GB") {
            $lang = 'English';
        }

        return $lang;
    }

    public function getAttachmentIds() {
        $json = json_decode($this->attachments, true);
        $ids = [];

        if ($json) {
            foreach ($json as $data) {
                $ids[] = $data;
            }
        }

        return $ids;
    }
}
