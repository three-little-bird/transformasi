<?php

namespace App\Console\Commands;

use App\Mail\Blast as MailBlast;
use App\Models\Blast;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Mail;

class BlastEmail extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'blast:email';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Email blast';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $blast = Blast::where('status', 'Queue')
            ->first();
        $blast->status = 'Processing';
        $blast->save();

        try {
            $emails = [];
            $recipients = json_decode($blast->recipients_context);
            foreach ($recipients as $recipient) {
                $emails = array_merge($emails, explode(',', $recipient->emails));
            }
    
            $emails = array_unique($emails);
    
            // foreach ($emails as $email) {
                Mail::to($emails)->send(new MailBlast($blast));
            // }
    
            $blast->status = 'Sent';
            $blast->save();

        } catch (\Exception $e) {
            $blast->status = 'Failed';
            $blast->message = $e->getMessage();
            $blast->save();
        }
        return 0;
    }
}
