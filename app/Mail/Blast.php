<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use App\Models\Blast as BlastModel;

class Blast extends Mailable
{
    use Queueable, SerializesModels;

    public $blast;
    public $recipient_email;
    public $to;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(BlastModel $blast)
    {
        $this->blast = $blast;
    }
    
    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $mail = $this->view('emails.default')
            ->subject($this->blast->subject)
            ->with([
                'content' => $this->blast->content
            ]);

        $attachments = json_decode($this->blast->attachments_context);
        foreach ($attachments as $attachment) {
            $mail->attachFromStorageDisk('local', $attachment);
        }

        return $mail;
    }
}
