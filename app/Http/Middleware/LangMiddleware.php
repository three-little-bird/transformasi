<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\App;
use Illuminate\Http\Request;

class LangMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if (in_array($request->hl, ['id', 'en'])) {
            App::setLocale($request->hl);
            session([
                'lang' => $request->hl
            ]);
        } elseif (in_array(session('lang'), ['id', 'en'])) {
            App::setLocale(session('lang'));
        }
        return $next($request);
    }
}
