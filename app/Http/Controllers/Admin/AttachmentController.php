<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Attachment;

class AttachmentController extends Controller
{
    public function index(Request $request)
    {
        $attachments = Attachment::when($request->search, function ($q) use ($request)
            {
                $q->where('title', 'LIKE', '%'.$request->search.'%');
            })
            ->latest()
            ->paginate(10)
            ->appends($request->all());
        return view('admin.pages.attachment', compact('attachments'));
    }

    public function add()
    {
        return view('admin.pages.attachment-add');
    }

    public function addAction(Request $request)
    {
        $request->validate([
            'title' => 'required',
            'file' => 'required|mimes:pdf,jpg,jpeg,png,gif,bmp,webp,zip,rar',
            'description' => 'required',
            'is_published' => 'required',
            'lang' => 'required'
        ]);

        $file_path = $request->file->store('images/pdf');

        $attachment = new Attachment();
        $attachment->title = $request->title;
        $attachment->description = $request->description;
        $attachment->file_path = $file_path;
        $attachment->is_published = $request->is_published;
        $attachment->download_count = 0;
        $attachment->lang = $request->lang;
        $attachment->save();

        return redirect()->route('admin.attachments')->with('success', 'Berhasil menambah lampiran');
    }

    public function edit($id)
    {
        $attachment = Attachment::findOrFail($id);
        return view('admin.pages.attachment-edit', compact('attachment'));
    }

    public function editAction(Request $request, $id)
    {
        $request->validate([
            'title' => 'required',
            'description' => 'required',
            'is_published' => 'required',
            'lang' => 'required'
        ]);

        $attachment = Attachment::findOrFail($id);

        if ($request->file) {
            $request->validate([
                'file' => 'required|mimes:pdf,jpg,jpeg,png,gif,bmp,webp,zip,rar',
            ]);
            $file_path = $request->file->store('images/pdf');
            $attachment->file_path = $file_path;
        }

        $attachment->title = $request->title;
        $attachment->description = $request->description;
        $attachment->is_published = $request->is_published;
        $attachment->lang = $request->lang;
        $attachment->save();

        return redirect()->route('admin.attachments')->with('success', 'Berhasil mengedit data lampiran');
    }

    public function deleteAction($id)
    {
        $attachment = Attachment::findOrFail($id);
        $attachment->delete();

        return back()->with('success', 'Berhasil menghapus lampiran');
    }
}
