<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Gallery;
use App\Models\GalleryPhoto;

class GalleryController extends Controller
{
    public function index()
    {
        $galleries = Gallery::with('photos')->latest()->paginate();
        return view('admin.pages.gallery', compact('galleries'));
    }

    public function add()
    {
        return view('admin.pages.gallery-add');
    }

    public function addAction(Request $request)
    {
        $request->validate([
            'title' => 'required',
            'lang' => 'required'
        ]);

        $gallery = new Gallery;
        $gallery->title = $request->title;
        $gallery->description = $request->description ? $request->description : '';
        $gallery->lang = $request->lang;
        $gallery->state = $request->is_published ? 1 : 0;
        $gallery->save();

        return redirect()->route('admin.galleries')->with('success', 'Berhasil menambah album');
    }

    public function edit($id)
    {
        $gallery = Gallery::findOrFail($id);
        return view('admin.pages.gallery-edit', compact('gallery'));
    }

    public function editAction(Request $request, $id)
    {
        $request->validate([
            'title' => 'required',
            'lang' => 'required'
        ]);

        $gallery = Gallery::findOrFail($id);
        $gallery->title = $request->title;
        $gallery->description = $request->description ? $request->description : '';
        $gallery->lang = $request->lang;
        $gallery->state = $request->is_published ? 1 : 0;
        $gallery->save();

        return redirect()->route('admin.galleries')->with('success', 'Berhasil mengedit data album');
    }

    public function deleteAction($id)
    {
        $gallery = Gallery::findOrFail($id);
        $gallery->delete();

        return back()->with('success', 'Berhasil menghapus album');
    }

    public function photo()
    {
        $photos = GalleryPhoto::with('gallery')->latest()->paginate();
        return view('admin.pages.gallery-photo', compact('photos'));
    }

    public function photoAdd()
    {
        $galleries = Gallery::whereIn('state', [0, 1])->get();
        return view('admin.pages.gallery-photo-add', compact('galleries'));
    }

    public function photoAddAction(Request $request)
    {
        $request->validate([
            'title' => 'required',
            'gallery_id' => 'required',
            'image' => 'required',
            'lang' => 'required'
        ]);

        $image = $request->image->store('images/gallery');

        $photo = new GalleryPhoto;
        $photo->title = $request->title;
        $photo->gallery_id = $request->gallery_id;
        $photo->image_path = $image;
        $photo->description = $request->description ? $request->description : '';
        $photo->alt = $request->alt;
        $photo->lang = $request->lang;
        $photo->save();

        return redirect()->route('admin.galleries-photo')->with('success', 'Berhasil menambah foto');
    }

    public function photoEdit($id)
    {
        $photo = GalleryPhoto::findOrFail($id);
        $galleries = Gallery::whereIn('state', [0, 1])->get();
        return view('admin.pages.gallery-photo-edit', compact('photo', 'galleries'));
    }

    public function photoEditAction(Request $request, $id)
    {
        $request->validate([
            'title' => 'required',
            'gallery_id' => 'required',
            'lang' => 'required'
        ]);

        $photo = GalleryPhoto::findOrFail($id);
        $photo->title = $request->title;
        $photo->gallery_id = $request->gallery_id;
        if ($request->image) {
            $request->validate([
                'image' => 'mimes:jpg,jpeg,png'
            ]);
            $image = $request->image->store('images/gallery');
            $photo->image_path = $image;
        }
        $photo->description = $request->description ? $request->description : '';
        $photo->alt = $request->alt;
        $photo->lang = $request->lang;
        $photo->save();

        return redirect()->route('admin.galleries-photo')->with('success', 'Berhasil mengedit data foto');
    }

    public function photoDeleteAction($id)
    {
        $photo = GalleryPhoto::findOrFail($id);
        $photo->delete();

        return back()->with('success', 'Berhasil menghapus foto');
    }
}
