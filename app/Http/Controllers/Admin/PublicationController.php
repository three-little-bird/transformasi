<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Attachment;
use Illuminate\Http\Request;
use App\Models\Publication;
use App\Models\Topic;

class PublicationController extends Controller
{
    public function index(Request $request)
    {
        $publications = Publication::when($request->search, function ($q) use ($request)
            {
                $q->where('title', 'LIKE', '%'.$request->search.'%');
            })
            ->latest()
            ->paginate(10)
            ->appends($request->all());
        return view('admin.pages.publication', compact('publications'));
    }

    public function add()
    {
        $types = [
            'book',
            'data',
            'infographic',
            'policy-brief'
        ];

        $topics = Topic::get();
        $attachments = Attachment::where('is_published', 1)->latest()->get();

        return view('admin.pages.publication-add', compact('types', 'topics', 'attachments'));
    }

    public function addAction(Request $request)
    {
        $request->validate([
            'title' => 'required',
            'type' => 'required',
            'image' => 'required|mimes:jpg,jpeg,png',
            'description' => 'required',
            'is_published' => 'required',
            'lang' => 'required'
        ]);

        if ($request->type == 'book') {
            $request->validate([
                'author' => 'required'
            ]);
        }

        if ($request->type == 'policy-brief') {
            $request->validate([
                'topic_id' => 'required'
            ]);

            // Add default publish date if type == policy brief
            $request->merge([
                'publish_date' => now()->format('Y-m-d')
            ]);
        } else {
            $request->validate([
                'publish_date' => 'required'
            ]);
        }

        $image = $request->image->store('images/publication');
        $attachments = [];
        foreach ($request->attachment as $attachment) {
            if (!empty($attachment)) {
                $attachments['attachment_' . (count($attachments) + 1)] = $attachment;
            }
        }

        $publication = new Publication();
        $publication->title = $request->title;
        $publication->type = $request->type;
        $publication->image_url = $image;
        $publication->publish_date = $request->publish_date;
        $publication->author = $request->author;
        $publication->topic_id = $request->topic_id;
        $publication->description = $request->description ? $request->description : '';
        $publication->is_published = $request->is_published ? 1 : 0;
        $publication->attachments = json_encode($attachments);
        $publication->lang = $request->lang;
        $publication->save();

        return redirect()->route('admin.publications')->with('success', 'Berhasil menambah publikasi');
    }

    public function edit($id)
    {
        $types = [
            'book',
            'data',
            'infographic',
            'policy-brief'
        ];

        $publication = Publication::findOrFail($id);

        $topics = Topic::get();
        $attachments = Attachment::where('is_published', 1)->latest()->get();

        return view('admin.pages.publication-edit', compact('types', 'publication', 'topics', 'attachments'));
    }

    public function editAction(Request $request, $id)
    {
        $request->validate([
            'title' => 'required',
            'type' => 'required',
            'description' => 'required',
            'is_published' => 'required',
            'lang' => 'required'
        ]);

        if ($request->type == 'book') {
            $request->validate([
                'author' => 'required'
            ]);
        }

        if ($request->type == 'policy-brief') {
            $request->validate([
                'topic_id' => 'required'
            ]);

            // Add default publish date if type == policy brief
            $request->merge([
                'publish_date' => now()->format('Y-m-d')
            ]);
        } else {
            $request->validate([
                'publish_date' => 'required'
            ]);
        }

        if ($request->image) {
            $request->validate([
                'image' => 'mimes:jpg,jpeg,png'
            ]);
            $image = $request->image->store('images/publication');
        } else {
            $image = null;
        }

        $attachments = [];
        foreach ($request->attachment as $attachment) {
            if (!empty($attachment)) {
                $attachments['attachment_' . (count($attachments) + 1)] = $attachment;
            }
        }

        $publication = Publication::findOrFail($id);
        $publication->title = $request->title;
        $publication->type = $request->type;
        if ($image) {
            $publication->image_url = $image;
        }
        $publication->publish_date = $request->publish_date;
        $publication->author = $request->author;
        $publication->topic_id = $request->topic_id;
        $publication->description = $request->description ? $request->description : '';
        $publication->is_published = $request->is_published ? 1 : 0;
        $publication->attachments = json_encode($attachments);
        $publication->lang = $request->lang;
        $publication->save();

        return redirect()->route('admin.publications')->with('success', 'Berhasil mengedit data publikasi');
    }

    public function deleteAction($id)
    {
        $publication = Publication::findOrFail($id);
        $publication->delete();

        return back()->with('success', 'Berhasil menghapus publikasi');
    }

    public function topic(Request $request)
    {
        $topics = Topic::when($request->search, function ($q) use ($request)
            {
                $q->where('name', 'LIKE', '%'.$request->search.'%');
            })
            ->latest()
            ->paginate(10)
            ->appends($request->all());

        return view('admin.pages.publication-topic', compact('topics'));
    }

    public function topicAdd()
    {
        return view('admin.pages.publication-topic-add');
    }

    public function topicAddAction(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'lang' => 'required'
        ]);

        $topic = new Topic;
        $topic->name = $request->name;
        $topic->lang = $request->lang;
        $topic->save();

        return redirect()->route('admin.publications.topics')->with('success', 'Berhasil menambah topik');
    }

    public function topicEdit($id)
    {
        $topic = Topic::findOrFail($id);
        return view('admin.pages.publication-topic-edit', compact('topic'));
    }

    public function topicEditAction(Request $request, $id)
    {
        $request->validate([
            'name' => 'required',
            'lang' => 'required'
        ]);

        $topic = Topic::findOrFail($id);
        $topic->name = $request->name;
        $topic->lang = $request->lang;
        $topic->save();

        return redirect()->route('admin.publications.topics')->with('success', 'Berhasil mengedit data topik');
    }

    public function topicDeleteAction($id)
    {
        $topic = Topic::findOrFail($id);
        $topic->delete();

        return back()->with('success', 'Berhasil menghapus topik');
    }
}
