<?php

namespace App\Http\Controllers\Admin;

use Auth;
use Illuminate\Support\Str;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Article;
use App\Models\Attachment;
use App\Models\Category;

class ArticleController extends Controller
{
    public function index(Request $request)
    {
        $articles = Article::with(['author', 'category'])
            ->when($request->search, function ($q) use ($request)
            {
                $q->where('title', 'LIKE', '%'.$request->search.'%');
            })
            ->when($request->sort, function ($q) use ($request)
            {
                $sort = explode(',', $request->sort);
                if ($sort && count($sort) == 2 && in_array($sort[0], ['id', 'title', 'status', 'category', 'author', 'lang', 'created_at', 'hits']) && in_array($sort[1], ['asc', 'desc'])) {
                    if ($sort[0] == 'id') {
                        $q->orderBy('id', $sort[1]);
                    } elseif ($sort[0] == 'title') {
                        $q->orderBy('title', $sort[1]);
                    } elseif ($sort[0] == 'status') {
                        $q->orderBy('state', $sort[1]);
                    } elseif ($sort[0] == 'category') {
                        $q->orderBy('category_id', $sort[1]);
                    } elseif ($sort[0] == 'author') {
                        $q->orderBy('created_by_id', $sort[1]);
                    } elseif ($sort[0] == 'lang') {
                        $q->orderBy('lang', $sort[1]);
                    } elseif ($sort[0] == 'created_at') {
                        $q->orderBy('created_at', $sort[1]);
                    } elseif ($sort[0] == 'hits') {
                        $q->orderBy('hits', $sort[1]);
                    }
                }
            })
            ->when(!$request->sort, function ($q) use ($request)
            {
                $q->latest();
            })
            ->whereIn('state', [0, 1])
            ->paginate(20)
            ->appends($request->all());
        return view('admin.pages.article', compact('articles'));
    }

    public function add()
    {
        $attachments = Attachment::where('is_published', 1)->latest()->get();
        $categories = Category::whereNotNull('parent_id')->orderBy('id')->get();
        return view('admin.pages.article-add', compact('attachments', 'categories'));
    }

    public function addAction(Request $request)
    {
        $request->validate([
            'title' => 'required',
            'category_id' => 'required',
            'lang' => 'required',
            'content' => 'required'
        ]);

        $user = Auth::user();
        $article = new Article;

        $attachments = [];
        foreach ($request->attachment as $attachment) {
            if (!empty($attachment)) {
                $attachments['attachment_' . (count($attachments) + 1)] = $attachment;
            }
        }

        if ($request->image) {
            $request->validate([
                'image' => 'required|mimes:jpg,jpeg,png'
            ]);
            $article->image_intro_path = $request->image->store('images/articles');

            try {
                $img = \Image::make(public_path($article->image_intro_path));
                $img->resize(720, null, function ($constraint) {
                    $constraint->aspectRatio();
                });
                $img->crop(720, 405);
                $article->thumbnail_url = 'images/thumbs/' . basename($article->image_intro_path);
    
                if(!\File::exists(public_path('images/thumbs'))) {
                    \File::makeDirectory(public_path('images/thumbs'), $mode = 0777, true, true);
                }
                $img->save(public_path($article->thumbnail_url));
            } catch (\Exception $e) {
                // $thumbnail_url = null;
            }
        }

        $article->title = $request->title;
        $article->slug = $request->slug ? $request->slug : Str::slug($request->title);
        $article->category_id = $request->category_id;
        $article->lang = $request->lang;
        $article->content = $request->content;
        $article->created_by_id = $user->id;
        $article->state = $request->state;
        $article->hits = 0;
        $article->attachments = json_encode($attachments);
        $article->metadata = json_encode($request->metadata);
        $article->save();

        return redirect()->route('admin.article')->with('success', 'Berhasil menambah artikel');
    }

    public function edit($id)
    {
        $article = Article::whereIn('state', [0, 1])->findOrFail($id);
        $attachments = Attachment::where('is_published', 1)->latest()->get();
        $categories = Category::whereNotNull('parent_id')->orderBy('id')->get();
        return view('admin.pages.article-edit', compact('article', 'attachments', 'categories'));
    }

    public function editAction(Request $request, $id)
    {
        $request->validate([
            'title' => 'required',
            'category_id' => 'required',
            'lang' => 'required',
            'content' => 'required'
        ]);

        $user = Auth::user();
        $article = Article::findOrFail($id);

        $attachments = [];
        foreach ($request->attachment as $attachment) {
            if (!empty($attachment)) {
                $attachments['attachment_' . (count($attachments) + 1)] = $attachment;
            }
        }

        if ($request->image) {
            $request->validate([
                'image' => 'mimes:jpg,jpeg,png'
            ]);
            $article->image_intro_path = $request->image->store('images/articles');

            try {
                $img = \Image::make(public_path($article->image_intro_path));
                $img->resize(720, null, function ($constraint) {
                    $constraint->aspectRatio();
                });
                $img->crop(720, 405);
                $article->thumbnail_url = 'images/thumbs/' . basename($article->image_intro_path);
    
                if(!\File::exists(public_path('images/thumbs'))) {
                    \File::makeDirectory(public_path('images/thumbs'), $mode = 0777, true, true);
                }
                $img->save(public_path($article->thumbnail_url));
            } catch (\Exception $e) {
                // $thumbnail_url = null;
            }
        }

        $article->title = $request->title;
        $article->slug = $request->slug ? $request->slug : Str::slug($request->title);
        $article->category_id = $request->category_id;
        $article->lang = $request->lang;
        $article->content = $request->content;
        $article->state = $request->state;
        $article->modified_by_id = $user->id;
        $article->attachments = json_encode($attachments);
        $article->metadata = json_encode($request->metadata);
        $article->save();

        return redirect()->route('admin.article')->with('success', 'Berhasil mengedit artikel');
    }

    public function deleteAction($id)
    {
        $user = Auth::user();

        $article = Article::findOrFail($id);
        $article->state = -2;
        $article->modified_by_id = $user->id;
        $article->save();

        return back()->with('success', 'Berhasil menghapus artikel');
    }
}
