<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Carousel;

class HomeCarouselController extends Controller
{
    public function index(Request $request)
    {
        $carousels = Carousel::when($request->search, function ($q) use ($request)
            {
                $q->where('title', 'LIKE', '%'.$request->search.'%');
            })
            ->latest()
            ->paginate(10)
            ->appends($request->all());

        return view('admin.pages.home-carousel', compact('carousels'));
    }

    public function add()
    {
        return view('admin.pages.home-carousel-add');
    }

    public function addAction(Request $request)
    {
        $request->validate([
            'file' => 'required|mimes:jpg,jpeg,png',
            'lang' => 'required'
        ]);

        $bg_path = $request->file->store('images/carousel');

        try {
            $img = \Image::make(public_path($bg_path));
            $img->resize(1280, null, function ($constraint) {
                $constraint->aspectRatio();
            });
            $img->crop(1280, 600);
            $img->save();
        } catch (\Exception $e) {
            //
        }

        $carousel = new Carousel();
        $carousel->title = $request->title;
        $carousel->description = $request->description;
        $carousel->bg_path = $bg_path;
        $carousel->link = $request->link;
        $carousel->lang = $request->lang;
        $carousel->is_published = $request->is_published ? 1 : 0;
        $carousel->save();

        return redirect()->route('admin.home-carousels')->with('success', 'Berhasil menambah carousel');
    }

    public function edit($id)
    {
        $carousel = Carousel::findOrFail($id);
        return view('admin.pages.home-carousel-edit', compact('carousel'));
    }

    public function editAction(Request $request, $id)
    {
        $request->validate([
            'lang' => 'required'
        ]);

        $carousel = Carousel::findOrFail($id);

        if ($request->file) {
            $request->validate([
                'file' => 'mimes:jpg,jpeg,png'
            ]);
            $bg_path = $request->file->store('images/carousel');
            
            try {
                $img = \Image::make(public_path($bg_path));
                $img->resize(1280, null, function ($constraint) {
                    $constraint->aspectRatio();
                });
                $img->crop(1280, 600);
                $img->save();
            } catch (\Exception $e) {
                //
            }

            $carousel->bg_path = $bg_path;
        }

        $carousel->title = $request->title;
        $carousel->description = $request->description;
        $carousel->link = $request->link;
        $carousel->lang = $request->lang;
        $carousel->is_published = $request->is_published ? 1 : 0;
        $carousel->save();

        return redirect()->route('admin.home-carousels')->with('success', 'Berhasil mengedit data carousel');
    }

    public function deleteAction($id)
    {
        $carousel = Carousel::findOrFail($id);
        $carousel->delete();

        return back()->with('success', 'Berhasil menghapus carousel');
    }
}
