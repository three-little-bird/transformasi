<?php

namespace App\Http\Controllers\Admin;

use Auth;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Models\Category;

class CategoryController extends Controller
{
    public function index(Request $request)
    {
        $categories = Category::when($request->search, function ($q) use ($request)
            {
                $q->where('name', 'LIKE', '%'.$request->search.'%');
            })
            ->latest()
            ->paginate(10)
            ->appends($request->all());

        return view('admin.pages.category', compact('categories'));
    }

    public function add()
    {
        $categories = Category::all();
        return view('admin.pages.category-add', compact('categories'));
    }

    public function addAction(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'lang' => 'required'
        ]);

        $user = Auth::user();

        $category = new Category;
        $category->name = $request->name;
        $category->slug = $request->slug ? $request->slug : Str::slug($request->name);
        $category->parent_id = $request->parent_id;
        $category->lang = $request->lang;
        $category->created_by_id = $user->id;

        if ($request->parent_id) {
            $parent = Category::findOrFail($request->parent_id);
            $level = $parent->level + 1;
            $path = $parent->path . "/" . $category->slug;
            $path = trim($path, '/');
        } else {
            $level = 1;
            $path = 1;
        }

        $category->level = $level;
        $category->path = $path;
        $category->save();

        return redirect()->route('admin.categories')->with('success', 'Berhasil menambah kategori');
    }

    public function edit($id)
    {
        $category = Category::findOrFail($id);
        $categories = Category::all();
        return view('admin.pages.category-edit', compact('category', 'categories'));
    }

    public function editAction(Request $request, $id)
    {
        $request->validate([
            'name' => 'required',
            'lang' => 'required'
        ]);

        $user = Auth::user();

        $category = Category::findOrFail($id);
        $category->name = $request->name;
        $category->slug = $request->slug ? $request->slug : Str::slug($request->name);
        $category->parent_id = $request->parent_id;
        $category->lang = $request->lang;
        $category->modified_by_id = $user->id;

        if ($request->parent_id) {
            $parent = Category::findOrFail($request->parent_id);
            $level = $parent->level + 1;
            $path = $parent->path . "/" . $category->slug;
            $path = trim($path, '/');
        } else {
            $level = 1;
            $path = 1;
        }

        $category->level = $level;
        $category->path = $path;
        $category->save();

        return redirect()->route('admin.categories')->with('success', 'Berhasil mengedit data kategori');
    }

    public function deleteAction($id)
    {
        $category = Category::findOrFail($id);
        $category->delete();

        return back()->with('success', 'Berhasil menghapus kategori');
    }
}
