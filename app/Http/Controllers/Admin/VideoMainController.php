<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Setting;
use App\Models\Video;

class VideoMainController extends Controller
{
    public function index()
    {
        $videoSettingId = Setting::where('key', 'home_video_id')
            ->first();
        $videoSettingEn = Setting::where('key', 'home_video_en')
            ->first();
        $videosId = Video::whereIn('lang', ['*', 'id-ID'])
            ->where('is_published', 1)
            ->get();
        $videosEn = Video::whereIn('lang', ['*', 'en-GB'])
            ->where('is_published', 1)
            ->get();
        $videoId = Video::find($videoSettingId->value);
        $videoEn = Video::find($videoSettingEn->value);
        return view('admin.pages.video-main', compact('videoId', 'videoEn', 'videosId', 'videosEn'));
    }

    public function update(Request $request, $lang)
    {
        if (in_array($lang, ['en', 'id'])) {
            $key = $lang == 'id' ? 'home_video_id' : 'home_video_en';
            $setting = Setting::where('key', $key)
                ->first();
            $setting->value = $request->video_id;
            $setting->save();
        }
        return back()->with('success', 'Berhasil mengubah data');
    }
}
