<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\MailingGroup;
use App\Models\MailingList;
use App\Models\Blast;
use Illuminate\Http\Request;

class MailingController extends Controller
{
    public function create()
    {
        $mailings = MailingGroup::withCount('mailingLists')
            ->latest()
            ->get();

        return view('admin.pages.mailing-create', compact('mailings'));
    }

    public function blast(Request $request)
    {
        $recipients = [];
        $attachments = [];

        foreach ($request->recipient_value as $key => $value) {
            $type = $request->recipient_type[$key];
            if ($type == 'email') {
                $emails = $value;
            } else {
                $emails = implode(",", MailingList::where('mailing_group_id', $value)->get()->pluck('email')->toArray());
            }
            $recipients[] = [
                'type' => $type,
                'value' => $value,
                'emails' => $emails
            ];
        }

        if ($request->attachments) {
            foreach ($request->attachments as $attachment) {
                $attachments[] = $attachment->store('blast-attachments', 'local');
            }
        }

        $blast = new Blast;
        $blast->subject = $request->subject;
        $blast->content = $request->content;
        $blast->recipients_context = json_encode($recipients);
        $blast->attachments_context = json_encode($attachments);
        $blast->save();

        return redirect()->route('admin.mailing.history')->with('success', 'Berhasil menambah blast');
    }

    public function lists(Request $request)
    {
        $mailings = MailingGroup::withCount('mailingLists')
            ->when($request->search, function ($q) use ($request)
            {
                $q->where('name', 'LIKE', '%'.$request->search.'%');
            })
            ->latest()
            ->paginate(10)
            ->appends($request->all());

        return view('admin.pages.mailing', compact('mailings'));
    }

    public function addList()
    {
        return view('admin.pages.mailing-add');
    }

    public function addListAction(Request $request)
    {
        $request->validate([
            'name' => 'required'
        ]);

        $mailing = new MailingGroup;
        $mailing->name = $request->name;
        $mailing->save();

        return redirect()->route('admin.mailing.lists')->with('success', 'Berhasil menambah mailing');
    }

    public function editList($id)
    {
        $mailing = MailingGroup::findOrFail($id);
        return view('admin.pages.mailing-edit', compact('mailing'));
    }

    public function editListAction(Request $request, $id)
    {
        $request->validate([
            'name' => 'required'
        ]);

        $mailing = MailingGroup::findOrFail($id);
        $mailing->name = $request->name;
        $mailing->save();

        return redirect()->route('admin.mailing.lists')->with('success', 'Berhasil mengedit data mailing');
    }

    public function deleteList($id)
    {
        $mailing = MailingGroup::findOrFail($id);
        $mailing->delete();

        return back()->with('success', 'Berhasil menghapus mailing list');
    }

    public function history(Request $request)
    {
        $blasts = Blast::when($request->search, function ($q) use ($request)
            {
                $q->where('subject', 'LIKE', '%'.$request->search.'%');
            })
            ->latest()
            ->paginate(10)
            ->appends($request->all());

        return view('admin.pages.mailing-history', compact('blasts'));
    }
}
