<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Donor;

class DonationController extends Controller
{
    public function index(Request $request)
    {
        $donors = Donor::when($request->search, function ($q) use ($request)
            {
                $q->where('name', 'LIKE', '%'.$request->search.'%');
            })
            ->latest()
            ->paginate(10)
            ->appends($request->all());

        return view('admin.pages.donation', compact('donors'));
    }
}
