<?php

namespace App\Http\Controllers\Admin;

use Auth;
use Illuminate\Support\Str;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Attachment;
use App\Models\Page;

class PageController extends Controller
{
    public function index(Request $request)
    {
        $pages = Page::when($request->search, function ($q) use ($request)
        {
            $q->where('title', 'LIKE', '%'.$request->search.'%');
        })
        ->when($request->sort, function ($q) use ($request)
        {
            $sort = explode(',', $request->sort);
            if ($sort && count($sort) == 2 && in_array($sort[0], ['id', 'title', 'status', 'author', 'lang', 'created_at', 'hits']) && in_array($sort[1], ['asc', 'desc'])) {
                if ($sort[0] == 'id') {
                    $q->orderBy('id', $sort[1]);
                } elseif ($sort[0] == 'title') {
                    $q->orderBy('title', $sort[1]);
                } elseif ($sort[0] == 'status') {
                    $q->orderBy('state', $sort[1]);
                } elseif ($sort[0] == 'author') {
                    $q->orderBy('created_by_id', $sort[1]);
                } elseif ($sort[0] == 'lang') {
                    $q->orderBy('lang', $sort[1]);
                } elseif ($sort[0] == 'created_at') {
                    $q->orderBy('created_at', $sort[1]);
                } elseif ($sort[0] == 'hits') {
                    $q->orderBy('hits', $sort[1]);
                }
            }
        })
        ->when(!$request->sort, function ($q) use ($request)
        {
            $q->latest();
        })
        ->whereIn('state', [0, 1])
        ->paginate(10);
        return view('admin.pages.page', compact('pages'));
    }

    public function add()
    {
        $attachments = Attachment::where('is_published', 1)->latest()->get();
        return view('admin.pages.page-add', compact('attachments'));
    }

    public function addAction(Request $request)
    {
        $request->validate([
            'title' => 'required',
            'layout' => 'required',
            'lang' => 'required',
            'content' => 'required'
        ]);

        $user = Auth::user();

        $attachments = [];
        foreach ($request->attachment as $attachment) {
            if (!empty($attachment)) {
                $attachments['attachment_' . (count($attachments) + 1)] = $attachment;
            }
        }

        $page = new Page;
        $page->title = $request->title;
        $page->slug = $request->slug ? $request->slug : Str::slug($request->title);
        $page->layout = $request->layout;
        $page->lang = $request->lang;
        $page->content = $request->content;
        $page->created_by_id = $user->id;
        $page->state = $request->state;
        $page->hits = 0;
        $page->attachments = json_encode($attachments);
        $page->metadata = json_encode($request->metadata);
        $page->save();

        return redirect()->route('admin.pages');
    }

    public function edit($id)
    {
        $page = Page::whereIn('state', [0, 1])->findOrFail($id);
        $attachments = Attachment::where('is_published', 1)->latest()->get();
        return view('admin.pages.page-edit', compact('page', 'attachments'));
    }

    public function editAction(Request $request, $id)
    {
        $request->validate([
            'title' => 'required',
            'layout' => 'required',
            'lang' => 'required',
            'content' => 'required'
        ]);

        $user = Auth::user();

        $attachments = [];
        foreach ($request->attachment as $attachment) {
            if (!empty($attachment)) {
                $attachments['attachment_' . (count($attachments) + 1)] = $attachment;
            }
        }

        $page = Page::whereIn('state', [0, 1])->findOrFail($id);
        $page->title = $request->title;
        $page->slug = $request->slug ? $request->slug : Str::slug($request->title);
        $page->layout = $request->layout;
        $page->lang = $request->lang;
        $page->content = $request->content;
        $page->modified_by_id = $user->id;
        $page->state = $request->state;
        $page->attachments = json_encode($attachments);
        $page->metadata = json_encode($request->metadata);
        $page->save();

        return redirect()->route('admin.pages')->with('success', 'Berhasil mengedit halaman');
    }

    public function deleteAction($id)
    {
        $user = Auth::user();

        $page = Page::findOrFail($id);
        $page->state = -2;
        $page->modified_by_id = $user->id;
        $page->save();

        return back()->with('success', 'Berhasil menghapus halaman');
    }
}
