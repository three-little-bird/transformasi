<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Video;

class VideoController extends Controller
{
    public function index(Request $request)
    {
        $videos = Video::when($request->search, function ($q) use ($request)
        {
            $q->where('title', 'LIKE', '%'.$request->search.'%');
        })
        ->latest()
        ->paginate(10)
        ->appends($request->all());

        return view('admin.pages.video', compact('videos'));
    }

    public function add()
    {
        return view('admin.pages.video-add');
    }

    public function addAction(Request $request)
    {
        $request->validate([
            'title' => 'required',
            'embedcode' => 'required',
            'lang' => 'required'
        ]);

        $video = new Video;
        $video->title = $request->title;
        $video->embedcode = $request->embedcode;
        $video->description = $request->description ? $request->description : '';
        $video->lang = $request->lang;
        $video->is_published = $request->is_published ? 1 : 0;
        $video->save();

        return redirect()->route('admin.videos')->with('success', 'Berhasil menambah vidio');
    }

    public function edit($id)
    {
        $video = Video::findOrFail($id);
        return view('admin.pages.video-edit', compact('video'));
    }

    public function editAction(Request $request, $id)
    {
        $request->validate([
            'title' => 'required',
            'embedcode' => 'required',
            'lang' => 'required'
        ]);

        $video = Video::findOrFail($id);
        $video->title = $request->title;
        $video->embedcode = $request->embedcode;
        $video->description = $request->description ? $request->description : '';
        $video->lang = $request->lang;
        $video->is_published = $request->is_published ? 1 : 0;
        $video->save();

        return redirect()->route('admin.videos')->with('success', 'Berhasil mengedit data vidio');
    }

    public function deleteAction($id)
    {
        $video = Video::findOrFail($id);
        $video->delete();

        return back()->with('success', 'Berhasil menghapus vidio');
    }
}
