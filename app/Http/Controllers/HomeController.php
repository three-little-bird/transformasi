<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Article;
use App\Models\Attachment;
use App\Models\Category;
use App\Models\Carousel;
use App\Models\Gallery;
use App\Models\GalleryPhoto;
use App\Models\Publication;
use App\Models\Page;
use App\Models\Topic;
use App\Models\Video;
use App\Models\Donor;
use App\Models\MailingList;
use App\Models\Setting;

class HomeController extends Controller
{
    public function index()
    {
        $carousels = Carousel::whereIn('lang', [getLocale(), '*'])
            ->where('is_published', 1)
            ->latest()
            ->get();
        $articles = Article::whereIn('lang', [getLocale(), '*'])
            ->whereIn('state', [0, 1])
            ->latest()
            ->limit(10)
            ->get();
        $publications = Publication::whereIn('lang', [getLocale(), '*'])
            ->where('type', 'book')
            ->latest()
            ->limit(4)
            ->get();

        $videoSetting = Setting::where('key', 'home_video_' . \Illuminate\Support\Facades\App::currentLocale())
            ->first();
        $video = Video::find($videoSetting->value);
        return view('web.pages.index', compact('carousels', 'articles', 'publications', 'video'));
    }

    public function articles(Request $request, $categorySlug = null) {
        $articles = Article::whereIn('lang', [getLocale(), '*'])
            ->when($categorySlug, function ($query) use ($categorySlug)
            {
                $category_id = explode("-", $categorySlug)[0];
                $query->where('category_id', $category_id);
            })
            ->when($request->q, function ($query) use ($request)
            {
                $query->where('title', 'LIKE', '%'.$request->q.'%');
            })
            ->where('state', 1)
            ->latest()
            ->paginate(6)
            ->appends($request->all());
        $categories = Category::withCount('articles')
            ->whereIn('lang', [getLocale(), '*'])
            ->get()
            ->sortByDesc(function ($category)
            {
                return $category->articles_count;
            })
            ->splice(0, 5);

        return view('web.pages.articles', compact('articles', 'categories'));
    }

    public function article($slug)
    {
        $article = Article::with(['author', 'category'])
            ->where('slug', $slug)
            ->where('state', 1)
            ->firstOrFail();
        $latests = Article::where('slug', '<>', $slug)
            ->where('state', 1)
            ->whereIn('lang', [getLocale(), '*'])
            ->limit(5)
            ->latest()
            ->get();
        $categories = Category::withCount('articles')
            ->whereIn('lang', [getLocale(), '*'])
            ->get()
            ->sortByDesc(function ($category)
            {
                return $category->articles_count;
            })
            ->splice(0, 5);

        // Get attachments
        $attachmentIds = $article->getAttachmentIds();
        $getAttachments = Attachment::whereIn('id', $attachmentIds)->get();
        $attachments = [];
        foreach ($getAttachments as $attachment) {
            $attachments[$attachment->id] = $attachment->toArray();
        }

        $article->hits = $article->hits + 1;
        $article->save();

        return view('web.pages.article', compact('article', 'latests', 'categories', 'attachments'));
    }

    public function page($slug)
    {
        $page = Page::where('slug', $slug)
            ->where('state', 1)
            ->firstOrFail();
        $page->hits = $page->hits + 1;
        $page->save();

        return view('web.pages.page', compact('page'));
    }

    public function publications(Request $request)
    {
        $publications = Publication::with('topic')
            ->where('is_published', 1)
            ->whereIn('lang', [getLocale(), '*'])
            ->when($request->q, function ($query) use ($request)
            {
                $query->where('title', 'LIKE', '%'.$request->q.'%');
            })
            ->when($request->type, function ($query) use ($request)
            {
                $query->where('type', $request->type);
            })
            ->when($request->author, function ($query) use ($request)
            {
                $query->where('author', $request->author);
            })
            ->when($request->topic_id, function ($query) use ($request)
            {
                $query->where('topic_id', $request->topic_id);
            })
            ->when($request->year, function ($query) use ($request)
            {
                $query->whereYear('created_at', $request->year);
            })
            ->latest()
            ->paginate(5)
            ->appends($request->all());
        
        $authors = Publication::select('author')
            ->groupBy('author')
            ->orderBy('author')
            ->pluck('author')
            ->toArray();

        $topics = Topic::whereIn('lang', [getLocale(), '*'])
            ->get();

        $typeText = [
            'policy-brief' => 'Policy Brief',
            'data' => 'Data',
            'infographic' => 'Infographic',
            'book' => 'Book',
        ];

        // Get attachments
        $attachmentIds = [];
        foreach ($publications as $publication) {
            $attachmentIds = array_merge($attachmentIds, $publication->getAttachmentIds());
        }
        $attachmentIds = array_unique($attachmentIds);
        $getAttachments = Attachment::whereIn('id', $attachmentIds)->get();
        $attachments = [];
        foreach ($getAttachments as $attachment) {
            $attachments[$attachment->id] = $attachment->toArray();
        }

        return view('web.pages.publication', compact('publications', 'typeText', 'authors', 'topics', 'attachments'));
    }

    public function publicationView(Request $request, $id)
    {
        $publication = Publication::with('topic')
            ->where('is_published', 1)
            ->whereIn('lang', [getLocale(), '*'])
            ->findOrFail($id);

        $typeText = [
            'policy-brief' => 'Policy Brief',
            'data' => 'Data',
            'infographic' => 'Infographic',
            'book' => 'Book',
        ];

        // Get attachments
        $attachmentIds = $publication->getAttachmentIds();
        $attachmentIds = array_unique($attachmentIds);
        $getAttachments = Attachment::whereIn('id', $attachmentIds)->get();
        $attachments = [];
        foreach ($getAttachments as $attachment) {
            $attachments[$attachment->id] = $attachment->toArray();
        }

        return view('web.pages.publication-view', compact('publication', 'typeText', 'attachments'));
    }

    public function attachmentDownload($id)
    {
        $attachment = Attachment::findOrFail($id);
        $attachment->download_count = $attachment->download_count + 1;
        $attachment->save();

        return response()->download(public_path($attachment->file_path));
    }

    public function galleries($id = null)
    {
        if ($id) {
            $gallery = Gallery::with('photos')
                ->whereIn('lang', [getLocale(), '*'])
                ->where('state', 1)
                ->findOrFail($id);
        } else {
            $gallery = Gallery::with('photos')
                ->whereIn('lang', [getLocale(), '*'])
                ->where('state', 1)
                ->latest()
                ->first();
        }

        $galleries = Gallery::withCount('photos')
            ->whereIn('lang', [getLocale(), '*'])
            ->where('state', 1)
            ->latest()
            ->get();

        return view('web.pages.galleries', compact('gallery', 'galleries'));
    }

    public function photoDownload($id)
    {
        $photo = GalleryPhoto::findOrFail($id);
        $photo->save();

        return response()->download(public_path($photo->image_path));
    }

    public function videos($year = null)
    {
        $getVideoYears = Video::select('created_at')
            ->whereIn('lang', [getLocale(), '*'])
            ->where('is_published', 1)
            ->latest()
            ->get();
        $years = [];
        foreach ($getVideoYears as $video) {
            $years[] = $video->created_at->format('Y');
        }
        $years = array_unique($years);
        if (!$year) {
            $year = $years[0];
        }
        $videos = Video::whereIn('lang', [getLocale(), '*'])
            ->where('is_published', 1)
            ->whereYear('created_at', $year)
            ->latest()
            ->paginate(5);

        return view('web.pages.videos', compact('videos', 'years'));
    }

    public function donate()
    {
        return view('web.pages.donate');
    }

    public function donateAction(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'email' => 'required',
            'phone' => 'required',
            'amount' => 'required',
            'verification' => 'required',
            'file' => 'required|mimes:jpg,jpeg,png'
        ]);

        $proof_path = $request->file->store('images/donor-ss-proof');

        $donor = new Donor();
        $donor->name = $request->name;
        $donor->email = $request->email;
        $donor->phone = $request->phone;
        $donor->amount = $request->amount;
        $donor->newsletter = $request->newsletter ? 1 : 0;
        $donor->agreement = $request->verification ? 1 : 0;
        $donor->proof_path = $proof_path;
        $donor->save();

        if ($donor->newsletter && !MailingList::where('email', $donor->email)->where('mailing_group_id', 1)->exists()) {
            $mailing = new MailingList();
            $mailing->mailing_group_id = 1;
            $mailing->name = $donor->name;
            $mailing->email = $donor->email;
            $mailing->save();
        }

        return back();
    }

    public function mailingSubscribe()
    {
        $message = session('newsletter-message');
        return view('web.pages.newsletter', compact('message'));
    }

    public function mailingSubscribeAction(Request $request)
    {
        $request->validate([
            'email' => 'required'
        ]);

        if ($request->email && !MailingList::where('email', $request->email)->where('mailing_group_id', 2)->exists()) {
            $mailing = new MailingList();
            $mailing->mailing_group_id = 2;
            $mailing->name = null;
            $mailing->email = $request->email;
            $mailing->save();
        }

        return redirect()->route('home.newsletter-subscribe')->with('newsletter-message', 'Email berhasil didaftarkan untuk berlangganan newsletter');
    }
}
